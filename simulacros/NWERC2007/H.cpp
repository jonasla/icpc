#include <bits/stdc++.h>

using namespace std;

typedef long long tint;

#define forsn(i,s,n) for(int i=int(s); i<int(n); i++)
#define dforn(i,n) for(tint i=tint(n)-1; i>= 0; i--)
#define forn(i,n) forsn(i,0,n)

const tint MAXN = 2048;
const tint MAXWORDS = 12000;
const tint MAXALFABETO = 32;

tint ways[MAXN];
string words[MAXWORDS];
tint padre[MAXN];
string p;

tint h[MAXALFABETO][2];
vector<tint> fit[MAXN];

void chequeo (tint c, tint &cantidad_correctas, tint signo)
{
	
	h[c][0] += signo;
	if (h[c][0] == h[c][1])
		cantidad_correctas++;
	else if (h[c][0] == h[c][1] + signo)
		cantidad_correctas--;
}

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint casos;
	cin >> casos;
	forn(caso,casos) // para cada caso
	{	
		tint w,n;
		cin >> p >> n; // leo la palabra grande
		w = tint(p.size()); 
		forn(q,MAXWORDS)
			words[q] = "";
		forn(i,MAXN) // limpio arreglo de dp
		{
			ways[i] = 0;
			padre[i] = -1;
			fit[i] = {};
		}
		
		forn(q,n) // por cada palabra del diccionario
		{
			cin >> words[q];
			
			forn(k,2) // limpio los histogramas iniciales
			forn(j,MAXALFABETO)
				h[j][k] = 0;
			
			tint r = tint(words[q].size());
				
			forn(i,r)
			{
				h[words[q][i]-'a'][1]++; // lleno el histograma de la palabra del diccionario
				if (i < r-1) // lleno (casi) todo el histograma del substring de la palabra grande
					h[p[i]-'a'][0]++;	
			}
			
			tint cantidad_correctas = 0;	
			forn(j,MAXALFABETO)
				cantidad_correctas += tint(h[j][0] == h[j][1]); // cuento la cantidad de correctas
				
			forsn(i,r-1,w) // para cada substring de largo r de la palabra grande
			{
				chequeo(p[i]-'a',cantidad_correctas,1); // agrego la letra del final histograma
				
				if (cantidad_correctas == MAXALFABETO && p[i-r+1] == words[q][0] && p[i] == words[q][r-1] ) // me fijo si matchea
					fit[i-r+1].push_back(q); // si matchea, entonces la puedo poner en i-r+1...i
				
				chequeo(p[i-r+1]-'a',cantidad_correctas,-1); // saco la letra del principio del histograma
			}
			
		}
		
		
		
		ways[w] = 1;
		
		dforn(i,w)
		{
			ways[i] = 0;
			for (const auto &z : fit[i])
			{
				tint largo = tint(words[z].size());
				ways[i] += ways[i+largo];
				ways[i] = min(ways[i],2LL);
				if (ways[i+largo] > 0)
					padre[i] = z;
			}
		}
		if (ways[0] == 0)
			cout << "impossible\n";
		else if (ways[0] > 1)
			cout << "ambiguous\n";
		else
		{
			tint k = 0;
			while (padre[k] != -1)
			{
				if (k)
					cout << " ";
				cout << words[padre[k]];
				k += tint(words[padre[k]].size());
			}
			cout << "\n";
		}
		
	}
	return 0;
}
