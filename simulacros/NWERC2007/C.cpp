#include <bits/stdc++.h>

using namespace std;

typedef long long tint;

#define forsn(i,s,n) for(tint i=tint(s); i<tint(n); i++)
#define forn(i,n) forsn(i,0,n)

tint techo(tint N, tint K) {
	return (N+K-1)/K;
}

const tint INF = 8*1e18;

int main() {
	tint T; cin >> T;
	while(T--) {
		tint N; cin >> N;
		tint M = techo(N, 5);
		
		tint MIN_AREA = INF;
		tint length = 0, width = 0;
		for (tint i=1; i*i<=M; i++) {
			tint w = i * 44 + 4;
			tint l = techo(M, i) * 10 + 2;
			tint a = w * l;
			if (MIN_AREA == a) {
				if (abs(l-w) < abs(length - width)) {
					MIN_AREA = a;
					length = l;
					width = w;
				} 
			} else if (MIN_AREA > a) {
				MIN_AREA = a;
				length = l;
				width = w;
			}
		}
		cout << max(length, width) << " X " << min(length, width) << " = " << length * width << endl;
	}
	return 0;
}
