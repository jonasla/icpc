#include <bits/stdc++.h>

using namespace std;

typedef long long tint;

#define forsn(i,s,n) for(int i=int(s); i<int(n); i++)
#define dforn(i,n) for(tint i=tint(n)-1; i>= 0; i--)
#define forn(i,n) forsn(i,0,n)

const tint INFINITO = 1000000000000000000;

vector<tint> dx = {-1,1,0,0};
vector<tint> dy = {0,0,1,-1};

void bfs ( tint i, tint j, const vector<vector<tint> > &grilla, vector<vector<tint> > &visitado, tint &ans, bool tutti)
{
	visitado[i][j] = 1;
	vector<pair<tint,tint> > vecinos = {{i,j}};
	vector<pair<tint,tint> > nuevosVecinos;
	tint maxi = -INFINITO, app = 0;
	tint n = tint(grilla.size());
	tint m = tint(grilla[0].size());
	while(!vecinos.empty())
	{
		pair<tint,tint> actual = vecinos.back();
		tint ri = actual.first;
		tint rj = actual.second;
		vecinos.pop_back();
		if (grilla[ri][rj] > maxi)
		{
			maxi = grilla[ri][rj];
			app = 1;
		}
		else if (maxi == grilla[ri][rj])
			app++;
		forn(z,4)
		{
			tint ni = ri + dx[z];
			tint nj = rj + dy[z];
			if (0 <= ni && ni < n && 0 <= nj && nj < m && !visitado[ni][nj])
			{
				nuevosVecinos.push_back({ni,nj});
				visitado[ni][nj] = 1;
			}
		}
		if (vecinos.empty())
		{
			swap(vecinos,nuevosVecinos);
			nuevosVecinos = {};
		}
	}
	ans += (app*((maxi > 0) or tutti));
	
	
}

int main()
{
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint casos;
	cin >> casos;
	forn(caso,casos)
	{
		tint n,m,d;
		cin >> n >> m >> d;
	
	
		vector<vector<tint> > grilla (n, vector<tint> (m));
		vector<vector<tint> > visitado (n, vector<tint> (m,0));
		tint maxii = -INFINITO;
		forn(i,n)
		forn(j,m)
		{
			cin >> grilla[i][j];
			grilla[i][j] -= (d-1);
			maxii = max(maxii,grilla[i][j]);
		}
		//~ cout << maxii << endl;
		forn(i,n)
		forn(j,m)
			if (maxii > 0)
				visitado[i][j] = (grilla[i][j] < 0);
		tint ans = 0;
		forn(i,n)
		forn(j,m)
			if (!visitado[i][j])
				bfs(i,j,grilla,visitado,ans, maxii <= 0);
				
		cout << ans << "\n";
			
		
	}	
	
	return 0;
}
