#include <bits/stdc++.h>

using namespace std;

typedef long long tint;

#define forsn(i,s,n) for(int i=int(s); i<int(n); i++)
#define dforn(i,n) for(tint i=tint(n)-1; i>= 0; i--)
#define forn(i,n) forsn(i,0,n)

const tint INFINITO = 1000000000000000000;

vector<tint> dx = {-1,1,0,0};
vector<tint> dy = {0,0,1,-1};

void bfs ( vector<pair<tint,tint> > &vecinos, vector<vector<tint> > &grilla, vector<vector<tint> > &visitado, vector<vector<tint> > &maxi, tint c)
{
	
	vector<pair<tint,tint> > nuevosVecinos;
	tint n = tint(grilla.size());
	tint m = tint(grilla[0].size());
	tint d = 0;
	while(!vecinos.empty())
	{
		pair<tint,tint> actual = vecinos.back();
		tint ri = actual.first;
		tint rj = actual.second;
		if ( c >= 0 )
			maxi[ri][rj] = d;
		else
			grilla[ri][rj] = d;
		vecinos.pop_back();
		
		forn(z,4)
		{
			tint ni = ri + dx[z];
			tint nj = rj + dy[z];
			if (0 <= ni && ni < n && 0 <= nj && nj < m && !visitado[ni][nj] && grilla[ni][nj] >= c)
			{
				nuevosVecinos.push_back({ni,nj});
				visitado[ni][nj] = 1;
			}
		}
		if (vecinos.empty())
		{
			swap(vecinos,nuevosVecinos);
			nuevosVecinos = {};
			d++;
		}
	}
	
	
	
}

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint casos;
	cin >> casos;
	forn(caso,casos)
	{
		tint n,x,y;
		cin >> n >> x >> y;
		tint xi,yi,xr,yr;
		cin >> xi >> yi >> xr >> yr;
		
	
	
		vector<vector<tint> > grilla (x, vector<tint> (y));
		vector<vector<tint> > visitado (x, vector<tint> (y,0));
		vector<pair<tint,tint> > start;
		forn(i,n)
		{
			tint xx,yy;
			cin >> xx >> yy;
			start.push_back({xx,yy});
			visitado[xx][yy] = 1;
		}
		bfs(start,grilla,visitado,grilla,-1);
		
		tint l = -1, r = x+y, lastAns = 0;
		while (r-l > 1)
		{
			tint c = (r+l)/2;
			vector<vector<tint> > maxi (x,vector<tint> (y,INFINITO));
			vector<pair<tint,tint>> vecinos = {{xi,yi}};
			forn(i,x)
			forn(j,y)
				visitado[i][j] = 0;
			if (grilla[xi][yi] >= c)
				bfs(vecinos,grilla,visitado,maxi,c);
				
			if (maxi[xr][yr] == INFINITO)
				r = c;
			else
			{
				l = c;
				lastAns = maxi[xr][yr];
			}
				
		}		
		cout << l << " " << lastAns << "\n";
			
		
	}	
	
	return 0;
}

