#include <bits/stdc++.h>

using namespace std;

typedef long long tint;

#define forsn(i,s,n) for(int i=int(s); i<int(n); i++)
#define forn(i,n) forsn(i,0,n)

const tint TFLOOR = 10;
const tint TCONV = 5;

int main() {
	tint T;
	cin >> T;
	while (T--) {
		tint H, L; cin >> H >> L;
		
		map<tint, pair<tint, tint>> spots;
		
		tint e;
		forn(i,H) {
			forn(j,L) {
				cin >> e;
				if (e != -1) {
					spots[e] = make_pair(i, j);
				}
			}
		}
		///
		//~ tint c_spot = 0;
		vector<tint> c_spots(H, 0);
		tint ans = 0;
		for (const auto &kv : spots) {
			auto ii = kv.second;
			
			tint diff_floor = ii.first;
			ans += diff_floor * TFLOOR * 2;
			
			
			tint diff_spot = min(
				abs(c_spots[ii.first] - ii.second), 
				L - abs(c_spots[ii.first] - ii.second)
			);
			
			c_spots[ii.first] = ii.second;
			
			ans += diff_spot * TCONV;
		}
		
		cout << ans << endl;
	}
	return 0;
}
