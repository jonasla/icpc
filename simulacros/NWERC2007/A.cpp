#include <bits/stdc++.h>

using namespace std;

typedef long long tint;

#define forsn(i,s,n) for(int i=int(s); i<int(n); i++)
#define forn(i,n) forsn(i,0,n)

struct Componente
{
	tint precio,calidad;
	Componente (tint pp, tint cc)
	{
		precio = pp;
		calidad = cc;
	}
};

bool operator < (Componente c1, Componente c2)
{
	return make_tuple(c1.precio,c1.calidad) < make_tuple(c2.precio,c2.calidad);
}

const tint INFINITO = 1000000000000000000;

int main()
{
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint casos;
	cin >> casos;
	forn(caso,casos)
	{
		tint n,b, t = 1;
		cin >> n >> b;
		map<string,tint> comp;
		vector<vector<Componente> > v;
		forn(i,n)
		{
			string type,name;
			tint price,quality;
			cin >> type >> name >> price >> quality;
			if (comp[type] == 0)
				comp[type] = t++;
			if ( comp[type] > tint(v.size()) )
				v.push_back({Componente(price,quality)});
			else
				v[comp[type]-1].push_back(Componente(price,quality));
		}	
		
			
		tint precio = 0, r = tint(v.size());
		forn(i,r)
		{
			sort(v[i].begin(),v[i].end());
			
			vector<Componente> aux = {v[i][0]};
			tint w = tint(v[i].size());
			tint q = 0;
			forsn(j,1,w)
				if (v[i][j].calidad > aux[q].calidad)
				{
					aux.push_back(v[i][j]);
					q++;
				}
			v[i] = aux;
				
			precio += v[i][0].precio;
			
		}
		
		vector<tint> indy (r,0);
		while (precio <= b)
		{
			pair<tint,tint> minima_calidad = {INFINITO,INFINITO};
			forn(i,r)
			{
				if (indy[i] < tint(v[i].size()) - 1 )
				{
					if (v[i][indy[i]].calidad < minima_calidad.first)
						minima_calidad = {v[i][indy[i]].calidad,i};
				}
			}
			tint elegido = minima_calidad.second;
			if (elegido != INFINITO && precio - v[elegido][indy[elegido]].precio + v[elegido][indy[elegido]+1].precio > b)
				break;
			else if (elegido != INFINITO)
			{
				precio += - v[elegido][indy[elegido]].precio + v[elegido][indy[elegido]+1].precio;
				indy[elegido]++;
			}
			else
				break;
			
		}
		tint ans = INFINITO;
		forn(i,r)
			ans = min(ans,v[i][indy[i]].calidad);
		cout << ans << "\n";
			
		
	}
	return 0;
}
