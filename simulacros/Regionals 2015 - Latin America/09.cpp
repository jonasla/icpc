#include <bits/stdc++.h>

using namespace std;

typedef long long tint;
typedef long double ldouble;

#define forsn(i,s,n) for (tint i = tint(s); i < (tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for (tint i = tint(n)-1; i >= 0LL; i--)
#define debug(x) cout << #x << "=" << x << endl

struct Arista
{
	tint llegada;
	ldouble B,A;
	Arista (tint ll, ldouble bb, ldouble aa)
	{
		llegada = ll;
		B = bb;
		A = aa;
	}
};

const ldouble INFINITO = 1e18;

void dijkstra (tint comienzo, vector<vector<Arista> > &ladj, vector<ldouble> &distance, ldouble t)
{
	priority_queue <pair<ldouble,tint> > q;
	tint n = distance.size();
	forn(i,n)
		distance[i] = (i != comienzo)*INFINITO;
	vector<tint> procesado (n,0);
	q.push({0,comienzo});
	while (!q.empty())
	{
		tint actual = q.top().second;
		q.pop();
		if (!procesado[actual])
		{
			procesado[actual] = 1;
			for (auto &vecino : ladj[actual])
			{
				if (distance[actual] + vecino.B + vecino.A*t < distance[vecino.llegada])
				{
					distance[vecino.llegada] = distance[actual] + vecino.B + vecino.A*t;
					q.push({-distance[vecino.llegada],vecino.llegada});
				}
			}
		}
	}
}

const ldouble epsilon = 1e-10;

ldouble f(ldouble t, vector<vector<Arista> >&ladj)
{
	tint n = tint(ladj.size());
	vector<ldouble> distance (n);
	dijkstra(0,ladj,distance,t);
	return distance[n-1];
}

ldouble maxiTernary (ldouble tL, ldouble tR, vector<vector<Arista> > &ladj)
{
	while (abs(tR-tL) > epsilon)
	{
		ldouble tLThird = (2.0*tL + tR)/3.0;
		ldouble tRThird = (tL + 2.0*tR)/3.0;
		
		if (f(tLThird,ladj) < f(tRThird,ladj))
			tL = tLThird;
		else
			tR = tRThird;
	}
	return f((tL+tR)/2.0,ladj);
}


int main()
{
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	tint n,m;
	while (cin >> n >> m)
	{
		vector<vector<Arista> > ladj(n);
		forn(i,m)
		{
			tint u,v;
			ldouble a,b;
			cin >> u >> v >> a >> b;
			u--;
			v--;
			ladj[u].emplace_back(v,b,a);
			ladj[v].emplace_back(u,b,a);
		}
		ldouble tL = 0.0, tR = 24.0*60.0;
		cout << fixed << showpoint << setprecision(5) << maxiTernary(tL,tR,ladj) << "\n";
	}
	return 0;
}
