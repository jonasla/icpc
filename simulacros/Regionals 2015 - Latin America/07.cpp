#include <bits/stdc++.h>

using namespace std;

typedef long long tint;

#define forsn(i,s,n) for (tint i = tint(s); i < (tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for (tint i = tint(n)-1; i >= 0LL; i--)
#define debug(x) cout << #x << "=" << x << endl

struct Bloque
{
	tint diferencia,precio;
	Bloque (tint dd, tint pp)
	{
		diferencia = dd;
		precio = pp;
	}
};

const tint MAXN = 1024;
bitset<MAXN> visto;


void bfs (tint start, tint d, vector<tint> &s, vector<Bloque> &vd, vector<Bloque> &vp, vector<vector<tint> > &ladj)
{
	tint dps = 0, ppp = 0, capa = (start >= d), suma = s[start];
	vector<tint> vecinos = {start}, nuevosVecinos;
	visto[start] = 1;
	
	while (!vecinos.empty())
	{
		dps += capa;
		ppp += 1-capa;
		tint actual = vecinos.back();
		vecinos.pop_back();
		for (const auto &x : ladj[actual])
			if (!visto[x])
			{
				nuevosVecinos.push_back(x);
				suma += s[x];
				visto[x] = 1;
			}
		if (vecinos.empty())
		{
			vecinos = nuevosVecinos;
			nuevosVecinos = {};
			capa ^= 1;
		}
	}
	if (dps > ppp)
		vd.emplace_back(dps-ppp,suma);
	else if (ppp > dps)
		vp.emplace_back(ppp-dps,suma);
}

tint mochila(vector<Bloque> &v, tint B)
{
	tint n = tint(v.size());
	vector<vector<tint> > best (n+1,vector<tint> (B+1,0));
	forsn(i,1,n+1)
	forn(b,B+1)
	{
		best[i][b] = best[i-1][b];
		if (v[i-1].precio <= b)
			best[i][b] = max(best[i][b],best[i-1][b-v[i-1].precio] + v[i-1].diferencia);
	} 
	return *max_element(best[n].begin(),best[n].end());
		
}

int main()
{
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	tint d,p,r,b;
	while (cin >> d >> p >> r >> b)
	{
		vector<tint> s (d+p);
		forn(i,d)
			cin >> s[i];
		forn(i,p)
			cin >> s[d+i];
		
		tint n = d+p;	
		vector<vector<tint> > ladj(n);
		forn(i,r)
		{
			tint x,y;
			cin >> x >> y;
			x--;
			y--;
			ladj[x].push_back(d+y);
			ladj[d+y].push_back(x);
		}
		visto.reset();
		vector<Bloque> vd;
		vector<Bloque> vp;
		forn(i,n)
			if (!visto[i])
				bfs(i,d,s,vd,vp,ladj);
		//~ for (auto bloque : vd)
			//~ cout << bloque.precio << " " << bloque.diferencia << endl;
		//~ cout << "---" << endl;
		//~ for (auto bloque : vp)
			//~ cout << bloque.precio << " " << bloque.diferencia << endl;
		//~ cout << "---" << endl;
		cout << d+mochila(vd,b) << " " << p+mochila(vp,b) << "\n";
		//~ cout << "-------" << endl;
	}
	return 0;
}
