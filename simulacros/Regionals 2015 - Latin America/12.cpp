#include <bits/stdc++.h>

using namespace std;

typedef long long tint;

#define forsn(i,s,n) for (tint i = tint(s); i < (tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for (tint i = tint(n)-1; i >= 0LL; i--)
#define debug(x) cout << #x << "=" << x << endl

const tint nmod = 1000000007;

tint mod (tint a)
{
	a %= nmod;
	if (a < 0)
		a += nmod;
	return a;
}

int main()
{
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	tint N,Q;
	while (cin >> N >> Q)
	{
		vector<vector<tint> > ways (N+1, vector<tint> (N+1,0));
		
		ways[1][1] = 1;
		forsn(n,2,N+1)
		forsn(k,1,N+1)
			ways[n][k] = mod(mod((n-k+1)*ways[n-1][k-1]) + mod((k)*ways[n-1][k]));
		forsn(n,1,N+1)
			forsn(k,1,N+1)
				ways[n][k] = mod(ways[n][k] + ways[n][k-1]);
		//~ forn(i,min(5LL,N+1))
		//~ {
			//~ forn(j,min(5LL,N+1))
				//~ cout << ways[i][j] << " ";
			//~ cout << endl;
		//~ }
		//~ cout << "----" << endl;
		forn(i,Q)
		{
			tint k;
			cin >> k;
			if (i)
				cout << " ";
			cout << ways[N][min(k,N)];
		}
		cout << "\n";
	}
	return 0;
}
