#include <bits/stdc++.h>

using namespace std;

typedef long long tint;

#define forsn(i,s,n) for (tint i = tint(s); i < (tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for (tint i = tint(n)-1; i >= 0LL; i--)
#define debug(x) cout << #x << "=" << x << endl

const tint CARTAS = 5;
const vector<tint> cartas = {1,10,100,1000,10000};

int main()
{
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	
	tint n,m;
	while (cin >> n >> m)
	{
		
		tint ans = 0;
		forn(round,m)
		{
			tint suma = 0, b, dedalo = 0;
			cin >> b;
			forn(i,n)
			{
				tint c;
				cin >> c;
				if (i > 0)
					suma += c;
				else
					dedalo = c;
			}
			
			tint gana = 0;
			if (suma + dedalo <= b)
				gana = dedalo;
				
			dforn(i,CARTAS)
				if (suma + cartas[i] <= b)
				{
					ans += cartas[i] - gana;
					break;
				}
		}
		cout << ans << "\n";
		
	}
	return 0;
}
