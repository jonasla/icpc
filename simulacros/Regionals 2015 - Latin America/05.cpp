#include <bits/stdc++.h>

using namespace std;

typedef long long tint;
typedef pair<tint, tint> ii;

#define forsn(i,s,n) for (tint i = tint(s); i < (tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for (tint i = tint(n)-1; i >= 0LL; i--)
#define debug(x) cout << #x << "=" << x << endl

struct Pto {
	tint x, y;
	Pto(tint xx, tint yy) : x(xx), y(yy) {}
	Pto() { x = 0, y = 0; }
};

Pto operator - (const Pto& p1, const Pto& p2) {
	return Pto(p1.x-p2.x, p1.y-p2.y);
}

tint operator ^ (const Pto& p1, const Pto& p2) {
	return p1.x*p2.y - p1.y*p2.x;
}

tint areaTri(const Pto& p1, const Pto& p2, const Pto& p3) {
	return abs((p1-p3)^(p1-p2));
}

tint areaPoli(const vector<Pto>& poli) {
	tint area = 0;
	tint n = poli.size();
	forn(i,n) 
		area += poli[i]^poli[(i+1)%n];
	return abs(area);
}

tint N;

tint modn(tint x) {
	return (x+N)%N;
}

int main() {
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	
	tint x, y; 
	while (cin >> N) {
		vector<Pto> poli;
		forn(i,N) {
			cin >> x >> y;
			poli.push_back(Pto(x,y));
		}
		///
		tint areaTotal = areaPoli(poli);
		tint currCarol = 0, currCarla = 0; 
		tint prevCarol = 0, prevCarla = 0;
		
		tint v = 0;
		tint w = 1;
		
		ii best = make_pair(1e18, 1e18);
		
		for (; v<N; v++) {
			if (v == 0) {
				currCarol = 0;
			} else {
				currCarol -= areaTri(poli[modn(v-1)], poli[modn(v)], poli[modn(w)]);
				w--;
				currCarol -= areaTri(poli[modn(v)], poli[modn(w)], poli[modn(w+1)]);
			}
			currCarla = areaTotal - currCarol;
			
			///
			
			while (currCarol * 2 < areaTotal) {
				prevCarol = currCarol;
				prevCarla = currCarla;
				
				w++;
				tint triag = areaTri(poli[modn(v)], poli[modn(w-1)], poli[modn(w)]);
				currCarol += triag;
				currCarla -= triag;				
			}
			
			ii curr = make_pair(currCarla, currCarol);
			ii prev = make_pair(min(prevCarol, prevCarla), max(prevCarol, prevCarla));
			
			ii bestV = max(curr, prev);
			best = min(best, bestV);
		}
		cout << best.second << " " << best.first << "\n";
	}
	return 0;
}
