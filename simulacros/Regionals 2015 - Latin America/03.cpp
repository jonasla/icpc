#include <bits/stdc++.h>

using namespace std;

typedef long long tint;

#define forsn(i,s,n) for (tint i = tint(s); i < (tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for (tint i = tint(n)-1; i >= 0LL; i--)
#define debug(x) cout << #x << "=" << x << endl

string U;
string ans;
int m;
int p;
int d;
bool saturar;

vector<int> used;

void imprimirVector (vector<int> v)
{
	if (!v.empty())
	{ 
		tint s = tint(v.size());
		cout << "[";
		forn(i,s-1)
			cout << v[i] << ",";
		cout << v[s-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}


bool tryToAdd() {
	if (used[d] < 2) {
		
		if (!(d == 0 && p == 0)) {			
			ans.push_back(d + '0');
			used[d]++;	
		//~ } else {
			//~ saturar = true;
		}
		
		m++;
		p++;
		
		return true;
	}
	return false;
} 

int digitP() {
	return U[p] - '0';
}

void retro() {
	p--;
	m--;
	d = digitP();
	used[d]--;
	ans.pop_back();
	
	d = digitP() - 1;
}

int main()
{
	while (cin >> U) {
		tint n = U.size();
		used = vector<int>(10, 0);
				
		ans = "";
		m = 0, p = 0, saturar = false;
		
		while (m < n) {
			
			if (!saturar) {
				d = digitP();
				bool added = false;
				
				while (!added) {						
					while (d >= 0 && !tryToAdd()) {
						d--;
						saturar = true;
					}
					
					if (d == -1) {
						retro();
					} else 
						added = true;
					
				}
			} else {
				d = 9;
				while (!tryToAdd()) d--;
				p++;
			}
		}
		cout << ans << endl;
	}
	return 0;
}
