#include <bits/stdc++.h>

using namespace std;

typedef long long tint;

#define forsn(i,s,n) for (tint i = tint(s); i < (tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for (tint i = tint(n)-1; i >= 0LL; i--)
#define debug(x) cout << #x << "=" << x << endl



int main()
{
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	tint T;
	while (cin >> T) {
		tint e, ans = 0;
		forn(i,5) {
			cin >> e;
			if (e == T) ans++;
		}
		cout << ans << endl;
	}
	return 0;
}
