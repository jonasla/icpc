#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}


const tint ZERO = 4096;
const tint MAXN = 8192;
const tint INFINITO = 8192*2;

tint a,b,c;
tint best[MAXN];



int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint n;
	while (cin >> n >> a >> b >> c)
	{
		forn(i,MAXN)
			best[i] = -INFINITO;
		best[ZERO] = 0;
		forsn(i,ZERO+1,ZERO+n+1)
			best[i] = 1 + max(best[i-a],max(best[i-b],best[i-c]));
		cout << best[ZERO+n] << "\n";
		
	}
	return 0;
}



