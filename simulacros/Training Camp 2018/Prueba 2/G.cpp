#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint n,sx,sy,ex,ey;
	while (cin >> n >> sx >> sy >> ex >> ey)
	{
		tint dx = ex - sx;
		tint dy = ey - sy;
		//~ cout << dx << " " << dy << endl; // 1 1 N E
		string s;
		cin >> s;
		tint cn = 0, cs = 0, ce = 0, cw = 0, ans = -1;
		bool llegue = false;
		forn(i,n+1)
		{
			if (dx >= 0 && dy >= 0)
				llegue |= ( cn >= abs(dy) ) && ( ce >= abs(dx) );
			else if (dx <= 0 && dy >= 0)
				llegue |= ( cn >= abs(dy) ) && ( cw >= abs(dx) );
			else if (dx >= 0 && dy <= 0)
				llegue |= ( cs >= abs(dy) ) && ( ce >= abs(dx) );
			else if (dx <= 0 && dy <= 0)
				llegue |= ( cs >= abs(dy) ) && ( cw >= abs(dx) );
			
			if (llegue)
			{
				ans = i;
				break;
			}
			if (i < n)
			{
				cn += (s[i] == 'N');
				cs += (s[i] == 'S');
				ce += (s[i] == 'E');
				cw += (s[i] == 'W');
			}
		} 
		cout << ans << "\n";
		
	}
	return 0;
}



