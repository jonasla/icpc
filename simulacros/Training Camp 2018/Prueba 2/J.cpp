#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <cmath>
#include <algorithm>
#include <cassert>
#include <tuple>
#include <functional>
#include <unordered_set>
#include <unordered_map>
#include <sstream>
#include <stdio.h>
#include <valarray>
#include <random>
#include <iomanip>


typedef long long tint;
typedef unsigned long long utint;
typedef long double ldouble; 


#define forn(i,n) for(tint i=0;i<(tint)(n); i++)
#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define esta(x,v) (find((v).begin(),(v).end(),(x)) !=  (v).end())
#define index(x,v) (find((v).begin(),(v).end(),(x)) - (v).begin())
#define debug(x) cout << #x << " = "  << x << endl



using namespace std;



void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}


const tint maxN = 512;
const tint INFINITO = 1e15;


struct Arista
{
	tint start,end,capacity,flow;
	Arista (tint ss, tint ee, tint cc, tint ff)
	{
		start = ss;
		end = ee;
		capacity = cc;
		flow = ff;
	}
};

vector<Arista>  red; // Red residual
vector<tint> ladj [maxN]; // Lista de adyacencia (guarda vecinos como indices en red)

tint n, s, t; // #Nodos, source, sink
tint ultimoVecino [maxN]; // ultimo vecino visitado en dfs
tint nivel [maxN]; // Nivel del bfs

void agregarArista (tint ss, tint ee, tint c)
{
	ladj[ss].push_back( tint (red.size())); // guardamos el indice
	red.push_back(Arista(ss,ee,c,0));
	ladj[ee].push_back( tint (red.size()));
	red.push_back(Arista(ee,ss,c,0));
	
}


bool bfs ()
{
	forn(i,n+1)
		nivel[i] = -1;
	vector<tint> vecinos = {s}, nuevosVecinos;
	nivel[s] = 0;
	while (!vecinos.empty() && nivel[t] == -1)
	{
		tint actual = vecinos.back();
		vecinos.pop_back();
		
		for (auto indiceArista : ladj[actual])
		{
			tint vecino = red[indiceArista].end;
			if (nivel[vecino] == -1 && red[indiceArista].flow < red[indiceArista].capacity) // Si bajo en uno el nivel y puedo mandar flujo en la red residual
			{
				nivel[vecino] = nivel[actual] + 1;
				nuevosVecinos.push_back(vecino);
			}
		}
		
		if (vecinos.empty())
		{
			swap(vecinos,nuevosVecinos);
			nuevosVecinos = {};
		}
	}
	return (nivel[t] != -1);
}

tint dfs (tint actual, tint flujo)
{
	if (flujo <= 0)
		return 0;
	else if (actual == t)
		return flujo;
	else
	{
		while (ultimoVecino[actual] < tint(ladj[actual].size()))
		{
			tint id = ladj[actual][ultimoVecino[actual]];
			
			if (nivel[red[id].end] == nivel[actual] + 1)
			{
				tint pushed = dfs (red[id].end,min(flujo, red[id].capacity - red[id].flow));
				
				if (pushed > 0)
				{
					red[id].flow += pushed;
					red[id^1].flow -= pushed;
					return pushed;
				}
			}
			ultimoVecino[actual]++;
			
		}
		return 0;	
	}
}

tint dinic ()
{
	tint flujo = 0;
	while (bfs())
	{
		
		forn(i,n+1)
			ultimoVecino[i] = 0;
		tint pushed = dfs(s,INFINITO);
		
		while (pushed > 0)
		{
			flujo += pushed;
			pushed = dfs(s,INFINITO);
		}
	}
	return flujo;
}

const tint MAXA = 1000500;
tint p[MAXA + 1] = {1,1};
tint a[maxN];

int main()
{
	#ifdef ACMTUYO
		assert(freopen("Dinic.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	for (tint i = 1; i <= MAXA; i++)
		if (p[i] == 1)
			for (tint j = i; j <= MAXA; j += i)
				p[j] = i;
	tint N;
	while (cin >> N)
	{
		n = 2*n+2;
		s = 0;
		t = 2*n+1;
		forn(i,n)
			cin >> a[i];
		
		forn(i,n)
		forn(j,n)
			if
		
	}
	return 0;
}


