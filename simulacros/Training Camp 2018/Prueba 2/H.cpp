#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

const tint MAXN = 524288;
const tint MAXQ = 524288;

tint N[MAXN];
tint S[MAXN];

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint n,q;
	while (cin >> n >> q)
	{
		forn(i,MAXN)
		{
			N[i] = 0;
			S[i] = 0;
		}
		vector<tint> notificaciones;
		tint maxT = 0;
		tint suma = 0;
		forn(k,q)
		{
			tint t,x;
			cin >> t >> x;
			if (t == 1)
			{
				suma++;
				notificaciones.emplace_back(x);
				N[x]++;
			}
			else if (t == 2)
			{
				suma -= N[x];
				S[x] += N[x];
				N[x] = 0;
			}
			else
			{
				if (x > maxT)
				{
					forsn(i,maxT,x)
					{
						if (S[notificaciones[i]] == 0) // leo notificacion
						{
							suma--;
							N[notificaciones[i]]--;
							//~ cout << "++++" << notificaciones[i] << endl;
						}
						else
							S[notificaciones[i]]--;
					}
					maxT = x;
				}
			}
			
			cout << suma << "\n";
		}
		//~ cout << "----" << endl << endl;
	}
	return 0;
}



