#include <bits/stdc++.h>

typedef long long tint;
typedef long double ldouble;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<ldouble> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << fixed << showpoint << setprecision(16) <<  v[i] << ",";
		cout << fixed << showpoint << setprecision(16) << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

struct Punto
{
	ldouble x,y;
	Punto (ldouble xx, ldouble yy)
	{
		x = xx;
		y = yy;
	}
};

const ldouble epsilon = 1e-8;

ldouble operator ^ (Punto p1, Punto p2)
{
	return p1.x*p2.y - p1.y*p2.x;
}

Punto operator - (Punto p1, Punto p2)
{
	return Punto(p1.x-p2.x,p1.y-p2.y);
}

ldouble norma (Punto p)
{
	return sqrt(p.x*p.x+p.y*p.y);
}

const ldouble INFINITO = 99999999999999999.0;

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada2.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint n;
	while (cin >> n)
	{
		vector<Punto> v (n,Punto(0.0,0.0));
		forn(i,n)
		{
			ldouble xx, yy;
			cin >> xx >> yy;
			v[i] = Punto(xx,yy);
		}
		ldouble mini = INFINITO;
		vector<ldouble> dist (n);
		forn(i,n)
			mini = min(mini,(((v[i]-v[(i+1)%n])^(v[(i+2)%n]-v[(i+1)%n]))/norma(v[(i+2)%n] - v[i])) );
		cout << fixed << showpoint << setprecision(16) << mini/2.0 << "\n";
	}
	return 0;
}



