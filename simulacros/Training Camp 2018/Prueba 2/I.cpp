#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

const tint MAXN = 32;
bitset<MAXN> visitado;

bool dfs (tint actual,tint padre, vector<vector<tint> > &ladj, tint b, tint origen)
{
	bool ans = false;
	visitado[actual] = 1;
	
	for (auto &vecino : ladj[actual])
	{
		
		if ( ((b & (1LL << vecino)) > 0) && (vecino != padre) )
		{
			ans |= (vecino == origen);
			if (!visitado[vecino])
				ans |= dfs(vecino,actual,ladj,b,origen);
		}
	}
	return ans;
	
}

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint n,m;
	while (cin >> n >> m)
	{
		vector<vector<tint> > ladj (n);
		forn(i,m)
		{
			tint a,b;
			cin >> a >> b;
			ladj[a-1].emplace_back(b-1);
			ladj[b-1].emplace_back(a-1);
		}
		
		tint ans = 0;
		forn(b,1LL << n)
		{
			if (__builtin_popcount(b) >= 3)
			{
				visitado.reset();
				forn(i,n)
					if ((b & (1LL << i)) > 0)
					{
						ans += dfs(i,-1,ladj,b,i);
						break;
					}
			}
		}
		cout << ans << "\n";
	}
	return 0;
}



