#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

void dfs(tint actual, vector<vector<tint> > &ladj, vector<tint> &visitado, tint &largo, bool &hayCiclo, tint padre)
{
	
	visitado[actual] = 1;
	largo++;
	//~ imprimirVector(estado);
	for (auto vecino : ladj[actual])
	{
		if (vecino != padre)
		{
			if (!visitado[vecino])
				dfs(vecino,ladj,visitado,largo,hayCiclo,actual);
			else
				hayCiclo = true;
		}
			
	}
	
}

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint n,m;
	while (cin >> n >> m)
	{
		vector<vector<tint> > ladj (n);
		forn(i,m)
		{
			tint a,b;
			cin >> a >> b;
			ladj[a-1].push_back(b-1);
			ladj[b-1].push_back(a-1);
		}
		vector<tint> visitado(n,0);
		tint ans = 0, caminos_impares = 0;
		
		forn(i,n)
			if (!visitado[i])
			{
				tint largo = 0;
				bool hayCiclo = false;
				dfs(i,ladj,visitado,largo,hayCiclo,-1); 
				if (hayCiclo)
					ans += (largo % 2);
				else
					caminos_impares += (largo % 2);
			}
		cout << ans + (caminos_impares%2) << "\n";
	}
	return 0;
}



