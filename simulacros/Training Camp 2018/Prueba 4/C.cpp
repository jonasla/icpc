#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

const tint HORAS = 24;
const tint MINUTOS = 60;

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint n;
	while (cin >> n)
	{
		vector<vector<tint> > hist (HORAS, vector<tint> (MINUTOS,0));
		forn(i,n)
		{
			tint h,m;
			cin >> h >> m;
			hist[h][m]++;
		}
		
		tint maxi = 0;
		forn(h,HORAS)
		forn(m,MINUTOS)
			maxi = max(maxi,hist[h][m]);
			
		cout << maxi << "\n";
		
	}
	return 0;
}



