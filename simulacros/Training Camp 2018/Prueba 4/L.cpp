#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

int main()
{
	assert(freopen("ascii.in", "r", stdin));
	assert(freopen("ascii.out", "w", stdout));
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint h,w, ans1 = 0, ans2 = 0;
	cin >> h >> w;
	forn(i,h)
	{
		string row;
		cin >> row;
		//~ cout << ans << endl;
		tint suma = 0;
		for (auto x : row)
		{
			suma += ((x == '/') || (x == '\\'));
			ans1 += (suma % 2)*(x == '.'); 
			ans2 += ((x == '/') + (x == '\\'));
		}
	}
	cout << ans1 + ans2/2 << "\n";
	return 0;
}



