import sys

n = int(sys.stdin.readline())
for case in range(n):
	word = sys.stdin.readline()[:-1]
	if len(word) > 10:
		print word[0] + str(len(word[1:-1])) + word[-1]
	else:
		print word
