#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint n,k;
	while (cin >> n >> k)
	{
		k--;
		tint creciente = 1;
		forn(i,k)
		{
			if (i)
				cout << " ";
			if (creciente)
			{
				cout << i/2+1;
				creciente ^= 1;
			}
			else
			{
				cout << n-i/2;
				creciente ^= 1;
			}
		}
		forsn(i,k,n)
		{
			if (i)
				cout << " ";
			if (creciente)
				cout << i-k/2+1;
			else
				cout << n+k/2-i+1;
		}
		cout << "\n";
	}
	return 0;
}



