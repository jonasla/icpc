#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

void dfs(tint actual, vector<vector<tint> > &ladj, vector<tint> &estado, vector<tint> &topo, bool &sePuede)
{
	
	estado[actual] = 1;
	//~ imprimirVector(estado);
	for (auto vecino : ladj[actual])
	{
		if (estado[vecino] == 1)
			sePuede = false;
		else if (estado[vecino] == 0)
			dfs(vecino,ladj,estado,topo,sePuede);
	}
	estado[actual] = 2;
	topo.push_back(actual);
}

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint c;
	while (cin >> c)
	{
		forn(caso,c)
		{
			tint n,h;
			cin >> n >> h;
			vector<tint> entran(n,0);
			vector<tint> salen(n,0);
			vector<vector<tint> > ladj(n);
			forn(i,h)
			{
				tint a,b;
				cin >> a >> b;
				ladj[a-1].push_back(b-1);
				entran[b-1]++;
				salen[a-1]++;
			}
			tint terminales = 0;
			forn(i,n)
				terminales += (salen[i] == 0);
			vector<tint> estado(n,0);
			vector<tint> topo;
			bool sePuede = true;
			forn(i,n)
				if (estado[i] == 0)
					dfs(i,ladj,estado,topo, sePuede);
			//~ debug(sePuede);
			if (sePuede && terminales == 1)
			{
				
				//~ imprimirVector(topo);
				vector<tint> caminos(n,0);
				caminos[topo[0]] = 1;
				forn(i,n)
					for (auto vecino : ladj[topo[i]])
						caminos[vecino] += caminos[topo[i]];
				if (caminos[topo[n-1]] > 1)
					cout << "missing hints\n";
				else
				{
					forn(i,n)
					{
						if (i)
							cout << " ";
						cout << topo[n-i-1]+1;
					}
					cout << "\n";
				}
				
			}
			else if (sePuede)
				cout << "missing hints\n";
			else
				cout << "recheck hints\n";
		}
	}
	return 0;
}



