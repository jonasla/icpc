import sys

k,q = map(int, sys.stdin.readline().split())

maxK = 1024
factorial = [1 for i in range(maxK)]

for i in range(2,maxK):
	factorial[i] = i*factorial[i-1]
	
for query in range(q):
	p = int(sys.stdin.readline())
	cInf = k-1 
	cSup = 50000*k
	while cSup - cInf > 1:
		n = (cSup + cInf) / 2
		
		s = k**n
		f = [0 for i in range(k+1)]
		for r in range(k):
			f[r] = r**n - sum([factorial[k]/factorial[z]/factorial[k-z] * f[z] for z in range(r)])
		#~ print f
		LHS = 2000*(s-f[k]) 
		RHS = p*s
		if LHS >= RHS:
			cSup = n
		else:
			cInf = n
	print cSup
