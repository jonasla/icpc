#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}



int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint n;
	while (cin >> n)
	{
		vector<vector<tint> > a (n,vector<tint> (n));
		forn(i,n)
		forn(j,n)
			cin >> a[i][j];
		
		vector<tint> x (n);
		forn(i,n)
		{
			cin >> x[i];
			x[i]--;
		}
		// PENSAMOS EL PROCESO INVERSO, Y SE VAN AGREGANDO
		reverse(x.begin(),x.end());
		vector<tint> ans(n,0);
		vector<tint> adentro;
		vector<vector<tint> > b (n, vector<tint> (n));
		// ENTONCES PODEMOS USAR DANTZIG
		forn(k,n)
		{
			// AGREGAMOS EL NUEVO
			adentro.push_back(x[k]);
			forn(i,k)
				b[i][k] = a[adentro[i]][adentro[k]];
			
			forn(j,k)
				b[k][j] = a[adentro[k]][adentro[j]];
			// ACTUALIZAMOS LA NUEVA COLUMNA
			forn(i,k)
			forn(j,k)
				b[i][k] = min(b[i][k],b[i][j] + b[j][k]);
			// ACTUALIZAMOS LA NUEVA FILA
			forn(j,k)
			forn(i,k)
				b[k][j] = min(b[k][j],b[k][i] + b[i][j]);
			// ACTUALIZMOS TODOS LOS PUNTOS ITNERMEDIOS
			forn(i,k)
			forn(j,k)
				b[i][j] = min(b[i][j], b[i][k] + b[k][j]);
			// CALCULAMOS LA SUMA QUE QUEDO
			tint suma = 0;
			forn(i,k+1)	
			forn(j,k+1)	
				suma += b[i][j];
			// RECORDAMOS QUE PENSAMOS EL PROCESO AL REVES
			ans[n-k-1] = suma;
		}
		forn(i,n)
		{
			if (i)
				cout << " ";
			cout << ans[i];
		}
		cout << "\n";
	}
	return 0;
}



