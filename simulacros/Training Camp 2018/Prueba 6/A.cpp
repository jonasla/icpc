#include <bits/stdc++.h>

typedef int tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint n;
	while (cin >> n)
	{
		
		vector<tint> a (n);
		tint suma = 0, maxi = 0;
		forn(i,n)
		{
			cin >> a[i];
			suma += a[i];
			maxi = max(maxi,a[i]);
		}
		if (maxi/2 > (suma-maxi))
			cout << (suma-maxi)*(n>2) << "\n";
		else
			cout << (suma/3)*(n>2) << "\n";
	}
	return 0;
}



