#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	string s = "ABRACADABRA";
	string w = "ABBRA";
	tint n = s.size();
	tint m = w.size();
	forn(i,n)
	forsn(j,i+1,n)
	forsn(k,j+1,n)
	forsn(l,k+1,n)
	{
		string aux = s.substr(i,j-i+1) + s.substr(k,l-k+1);
		//~ debug(i);
		//~ debug(j);
		//~ debug(k);
		//~ debug(l);
		//~ debug(aux);
		if (aux == w)
			cout << "SE PUEDE" << endl;
	}
	return 0;
}



