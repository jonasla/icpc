#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	string s;
	while (cin >> s)
	{
		tint r, ans = 0;
		cin >> r;
		forn(palabra,r)
		{
			string w;
			cin >> w;
			tint n = tint(s.size()), m = tint(w.size());
			if ( m <= n )
			{
				tint i = 0, j = 0, k = 0;
				while (i < n) 
				{
					
					tint k_1 = 0, i_anterior = i;
					while (i < n && k_1 < m && s[i] == w[k_1])
					{
						i++;
						k_1++;
					}
					if (k_1 < k)
						j = i_anterior+1;
					else
					{
						j = i+1;
						k = max(k_1,k);
					}
					//~ debug(i);
					//~ debug(j);
					//~ debug(k);
					//~ cout << "----" << endl;
					tint k_2 = k;
					while (j < n && k_2 < m && s[j] == w[k_2])
					{
						j++;
						k_2++;
					}
					if (k_2 == m)
					{
						ans++;
						break;
					} 
					i++;
					j = i;
				}
				
					
			}
		}
		cout << ans << "\n";
	}
	return 0;
}



