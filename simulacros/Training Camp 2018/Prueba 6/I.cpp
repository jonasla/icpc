#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint n;
	while (cin >> n)
	{
		vector<vector<vector<tint> > > inter (n, vector<vector<tint> > (n));
		vector<set<tint> > v (n);
		forn(i,n*(n-1)/2)
		{
			tint x,y,k;
			cin >> x >> y >> k;
			forn(j,k)
			{
				tint a;
				cin >> a;
				inter[x-1][y-1].push_back(a);
				inter[y-1][x-1].push_back(a);
				v[x-1].insert(a);
				v[y-1].insert(a);
			}
		}
		bool todo_piola = true;
		forn(i,n)
		forn(j,i)
		{
			set<tint> f;
			for (auto &x : v[i])
				if (v[j].find(x) != v[j].end())
					f.insert(x);

			todo_piola &= (tint(f.size()) == tint(inter[i][j].size()) );
		}
		if (todo_piola)
		{
			cout << "Yes\n";
			forn(i,n)
			{
				cout << tint(v[i].size());
				for (auto &x : v[i])
					cout << " " << x;
				cout << "\n";
			}
		}
		else
			cout << "No\n";
			
		
	}
	return 0;
}



