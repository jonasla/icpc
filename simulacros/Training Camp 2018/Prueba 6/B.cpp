#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

map<char,char> proximo = {	{'0','1'},{'1','2'},{'2','3'},{'3','4'},{'4','5'},{'5','6'}, {'6','7'},{'7','8'},{'8','9'},{'9','A'},{'A','B'},{'B','C'}, {'C','D'},{'D','E'},{'E','F'},{'F','0'} };
				
map<char,tint> contador;
map<char,vector<tint> > apariciones;
bool todos_distintos(const string &s)
{
	contador = {};
	bool todo_piola = true;
	for (auto &x : s)
	{
		contador[x]++;
		todo_piola &= (contador[x] < 2);
	}
	return todo_piola;
}			

void siguiente (string &s, tint i, bool corro)
{
	if (i == -1)
		s = "1" + s;
	else
	{
		tint n = s.size();
		s[i] = proximo[s[i]];
		if (corro)
			forsn(j,i+1,n)
				s[j] = '0';
		if (s[i] == '0')
			siguiente(s,i-1,false);
	}
}

void siguiente_candidato(string &s)
{
	apariciones = {};
	tint n = tint(s.size());
	if (n >= 2 && s[0] == 'F' && s[1] == 'F')
		s = "100" + s.substr(2,n);
	else
	{
		forn(i,n)
		{
			apariciones[s[i]].push_back(i);
			if (tint(apariciones[s[i]].size()) >= 2)
			{
				siguiente(s,i,true);
				break;
			}
		}
	}
	
}


int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	string s;
	while (cin >> s)
	{
		tint n = tint(s.size());
		siguiente(s,n-1,true);
		
		while (!todos_distintos(s))
			siguiente_candidato(s);
		cout << s << "\n";
	}
	return 0;
}



