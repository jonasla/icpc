#include <bits/stdc++.h>

#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

typedef long long tint;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

const tint MAXN = 1024;
tint d[MAXN][MAXN][2];
char p[MAXN][MAXN][2];

const tint INFINITO = 1000000000000000;

tint valuacion (tint x,tint k)
{
	tint v = 0;
	while (x % k == 0)
	{
		x /= k;
		v++;
	}
	return v;
}

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint n;
	while (cin >> n)
	{
		pair<tint,tint> zero = {-1,-1};
		
		forn(i,n)
		forn(j,n)
		{
			tint x;
			cin >> x;
			if (i == 0)
			{
				forn(k,2)
					p[i][j][k] = 'R';
			}
			if (j == 0)
			{
				forn(k,2)
					p[i][j][k] = 'D';
			}
			if (x == 0 && zero.first == -1)
				zero = {i,j};
			else if (x != 0)
			{
				d[i][j][0] = valuacion(x,2);
				d[i][j][1] = valuacion(x,5);
			}
			else
			{
				d[i][j][0] = INFINITO;
				d[i][j][1] = INFINITO;
			}
			
		}
		
		forn(k,2)
		forn(i,n)
		forn(j,n)
		{
			if (i != 0 || j != 0)
			{
				tint a = INFINITO, b = INFINITO;	
				if (i > 0)
					a = d[i-1][j][k];
				if (j > 0)
					b = d[i][j-1][k];
				if (a < b)
					p[i][j][k] = 'D';
				else
					p[i][j][k] = 'R';
				d[i][j][k] += min(a,b);
			}
			
		}
		tint r = (d[n-1][n-1][1] < d[n-1][n-1][0]);
		tint end_i = n-1, end_j = n-1;
		vector<char> ans;
		
		forn(i,2*n-2)
		{
			char w = p[end_i][end_j][r];
			ans.emplace_back(w);
			if (w == 'R')
				end_j--;
			else
				end_i--;
		}
		
		tint risposta = d[n-1][n-1][r];
		
		if (zero.first >= 0 && risposta > 1)
			risposta = 1;
		cout << risposta << "\n";
		if (zero.first >= 0 && risposta == 1)
		{
			forn(k,zero.first)
				cout << "D";
			forn(k,n-1)
				cout << "R";
			forn(k,n-zero.first-1)
				cout << "D";
		}
		else
		{
			dforn(i,2*n-2)
				cout << ans[i];
		}
		
		cout << "\n";
	}
	
	
	return 0;
}




