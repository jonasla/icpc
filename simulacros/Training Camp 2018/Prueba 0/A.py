import sys

n = int(sys.stdin.readline())
l = map(int,sys.stdin.readline().split())
l.sort()

sePuede = False
for i in range(1,n):
	x = 2**(l[i-1]) - 1
	sePuede = sePuede or (x*x >= 2**l[i]) 
	
	
if sePuede:
	print "YES"
else:
	print "NO"
