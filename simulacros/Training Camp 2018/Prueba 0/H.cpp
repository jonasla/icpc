#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint n;
	while (cin >> n)
	{
		map<tint,tint> h;
		map<tint,tint> v;
		map<tint,map<tint,tint> > r;
		forn(i,n)
		{
			tint x,y;
			cin >> x >> y;
			h[x]++;
			v[y]++;
			r[x][y]++;
		}
		tint ans = 0;
		for (auto x : h)
			ans += x.second*(x.second-1)/2;
		for (auto x : v)
			ans += x.second*(x.second-1)/2;
		for (auto z : r)
		for (auto x : z.second)
			ans -= x.second*(x.second-1)/2;
		
		cout << ans << "\n";
		
	}
	return 0;
}



