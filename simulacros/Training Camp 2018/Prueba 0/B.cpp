#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

struct Risposta
{
	tint a,b,c,d;
	Risposta (tint aa, tint bb, tint cc, tint dd)
	{
		a = aa;
		b = bb;
		c = cc;
		d = dd;
	}
};

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint n;
	while (cin >> n)
	{
		vector<vector<tint> > data (n, vector<tint> (3));
		forn(i,n)
			cin >> data[i][0] >> data[i][1] >> data[i][2];
		vector<Risposta> ans;
		forn(a,10)
		forn(b,10)
		forn(c,10)
		forn(d,10)
			if (a != b && a != c && a != d && b != c && b != d && c != d)
			{
				bool data_consistente = true;
				forn(i,n)
				{
					
					tint d1 = data[i][0]/1000;
					tint d2 = (data[i][0]/100)%10;
					tint d3 = (data[i][0]/10)%10;
					tint d4 = data[i][0]%10;
					tint posicion_y_numero  = (a == d1) + (b == d2) + (c == d3) + (d == d4);
					tint numero_no_posicion = (a == d2) + (a == d3) + (a == d4) + (b == d1) + (b == d3) + (b == d4) + (c == d1) + (c == d2) + (c == d4) + (d == d1) + (d == d2) + (d == d3);
					data_consistente &= (data[i][1] == posicion_y_numero) && (data[i][2] == numero_no_posicion);
				}
				if (data_consistente)
					ans.push_back(Risposta(a,b,c,d));
			}
		tint r = tint(ans.size());
		if (r == 0)
			cout << "Incorrect data\n";
		else if (r == 1)
			cout << ans[0].a << ans[0].b << ans[0].c << ans[0].d << "\n";
		else
			cout << "Need more data\n";
	}
	return 0;
}



