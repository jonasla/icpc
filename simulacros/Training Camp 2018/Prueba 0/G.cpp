#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

const tint MAXN = 1LL << 20;
const tint MAXA = 1LL << 20;
bitset<MAXA> esta;
tint a[MAXN]; // entrada
tint indy[MAXA]; // indices
tint clique[MAXN]; // mayor clique en  el prefijo

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint n;
	while (cin >> n)
	{
		esta.reset();
		forn(i,n)
		{
			cin >> a[i];
			esta[a[i]] = 1;
			indy[a[i]] = i;
			clique[i] = 0;
		}
		tint maxi = *max_element(a,a+n);
		forn(i,n)
		{
			clique[i]++;
			tint x = 2*a[i];
			while (x <= maxi)
			{
				if (esta[x])
					clique[indy[x]] = max(clique[indy[x]],clique[i]);
				x += a[i];
			}
			
		}
		cout << *max_element(clique,clique+n) << "\n";
		
	}
	return 0;
}



