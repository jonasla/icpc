#include <bits/stdc++.h>

typedef long long tint;
typedef double ldouble;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

struct Particula
{
	ldouble x,v;
	Particula (ldouble xx, ldouble vv)
	{
		x = xx;
		v = vv;
	}
	Particula()
	{
		x = 0;
		v = 0;
	}
};

bool operator < (Particula p1, Particula p2)
{
	return make_pair(p1.x,p1.v) < make_pair(p2.x,p2.v);
}

const tint MAXN = 524288;
const ldouble INFINITO = 1000000001.0;
const ldouble epsilon = 1e-9;

Particula v[MAXN];

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	//~ ios_base::sync_with_stdio(0);
	//~ cin.tie(NULL);
	tint n;
	//~ while (cin >> n)
	//~ {
		assert(scanf("%lld",&n));
		forn(i,n)
		{
			ldouble xx,vv;
			//~ cin >> xx >> vv;
			assert(scanf("%lf%lf",&xx,&vv));
			v[i] = Particula(xx,vv);
		}
		tint start = 0, end = n-1;
		while(v[start].v < 0)
			start++;
		while(v[end].v > 0)
			end--;
		n = tint(end-start+1);
		if (n > 1)
		{
			ldouble cInf = 0.0; // no hay cruce
			ldouble cSup = (v[end].x - v[start].x)/(v[start].v-v[end].v); // mira que no se van a cruzar en este tiempo con las cotas del enunciado
			while (cSup - cInf > epsilon)
			{
				ldouble t = (cSup + cInf) / 2.0;
				ldouble maxi = -INFINITO;
				bool hayCruce = false;
				forsn(i,start,end+1)
				{
					if (hayCruce)
						break;
					if (v[i].v > 0)
						maxi = max(maxi,v[i].x + t*v[i].v);
					else
						hayCruce |=  (maxi >= v[i].x + t*v[i].v);
				}
				if (hayCruce)
					cSup = t;
				else
					cInf = t;
			}
			ldouble ans = (cSup + cInf) / 2.0;
			printf("%.16f\n",ans);
			//~ cout << fixed << showpoint << setprecision(16) << ans  << "\n";
		}
		else
			printf("-1\n");
			//~ cout << "-1\n";
		
	//~ }
	return 0;
}



