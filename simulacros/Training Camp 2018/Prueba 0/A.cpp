#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}


int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint n;
	while (cin >> n)
	{
		vector<tint> a (n);
		forn(i,n)
			cin >> a[i];
		sort(a.begin(),a.end());
		bool noSePuede = true;
		forn(i,n-1)
			if (a[i] != 1 && a[i] < a[i+1])
				noSePuede &= (2*a[i] <= a[i+1]);
		if (noSePuede)
			cout << "NO\n";
		else
			cout << "YES\n";
	}
	return 0;
}



