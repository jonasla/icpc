#include <bits/stdc++.h>

typedef long long tint;
typedef long double ldouble;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}


struct Punto
{
	ldouble x,y;
	Punto (ldouble xx, ldouble yy)
	{
		x = xx;
		y = yy;
	}
};

void imprimir ( Punto p )
{
	cout << fixed << showpoint << setprecision(16) << p.x << " " << p.y << "\n";
}

Punto operator + (Punto p1, Punto p2)
{
	return Punto(p1.x+p2.x,p1.y+p2.y);
}

Punto operator * (ldouble s, Punto p)
{
	return Punto(s*p.x,s*p.y);
}

ldouble norma (Punto p)
{
	return sqrt(p.x*p.x+p.y*p.y);
}

Punto operator ~ (Punto p)
{
	return Punto(-p.y,p.x);
}



int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	ldouble px,py,vx,vy,a,b,c,d;
	while ( cin >> px >> py >> vx >> vy >> a >> b >> c >> d )
	{
		Punto P = Punto (px,py);
		Punto V = Punto(vx,vy);
		V = (1.0/norma(V))*V;
		Punto VP = ~V;
		
		Punto A = P + b*V;
		Punto B = P + (a/2.0)*VP;
		Punto C = P + (c/2.0)*VP;
		Punto D = P + (c/2.0)*VP + (-d)*V;
		Punto E = P + (-c/2.0)*VP + (-d)*V;
		Punto F = P + (-c/2.0)*VP;
		Punto G = P + (-a/2.0)*VP;
		
		imprimir(A);
		imprimir(B);
		imprimir(C);
		imprimir(D);
		imprimir(E);
		imprimir(F);
		imprimir(G);
		
	}
	
	return 0;
}



