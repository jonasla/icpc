#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint n;
	while (cin >> n)
	{
		tint k = 0, nOrig = n;
		while (nOrig > 0)
		{
			nOrig /= 4;
			k++;
		}
		vector<tint> a (n);
		forn(i,n)
			cin >> a[i];
			
		sort(a.rbegin(),a.rend());
		vector<tint> s (n+1,0);
		forn(i,n)
			s[i+1] = s[i] + a[i];
			
		tint ans = 0, r = 1;
		forn(i,k)
		{
			ans += s[r];
			r *= 4;
		}
		cout << ans << "\n";
	}
	return 0;
}



