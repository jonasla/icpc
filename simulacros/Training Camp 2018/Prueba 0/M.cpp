#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

const tint INFINITO = 999999999999999;

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint n;
	while (cin >> n)
	{
		vector<tint> a (n);
		forn(i,n)
			cin >> a[i];
		
		vector<pair<tint,tint> > ans;
		forn(k,n)
		{
			tint mini = INFINITO;
			tint indy = INFINITO;
			forsn(i,k,n)
				if (a[i] < mini)
				{
					mini = a[i];
					indy = i;
				}
			ans.emplace_back(k,indy);
			swap(a[k],a[indy]);
		}
		cout << tint(ans.size()) << "\n";
		for (auto x : ans)
			cout << x.first << " " << x.second << "\n";
	}
	return 0;
}



