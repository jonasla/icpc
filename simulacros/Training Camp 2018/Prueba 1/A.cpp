#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint m,s;
	while (cin >> m >> s)
	{
		
		vector<tint> s1 (m,9);
		vector<tint> s2 (m,0);
		if (s <= 9*m)
		{
			
			// maximo
			tint r1 = 9*m;
			tint p1 = m-1;
			while(r1 > s)
			{
				if ( (r1-s) <= 9)
				{
					s1[p1--] = 9-(r1-s);
					r1 = s; 
				}
				else
				{
					s1[p1--] = 0;
					r1 -= 9;
				}
			}
			// minima 
			tint r2 = 1;
			tint p2 = m-1;
			s2[0] = 1;
			while (r2 < s)
			{
				if ((s-r2) <= 9)
				{
					s2[p2--] += (s-r2);
					r2 = s;
				}
				else
				{
					s2[p2--] = 9;
					r2 += 9;
				}
			}
		}
		if (s > 9*m)
			cout << "-1 -1\n";
		else if (s == 0 && m == 1)
			cout << "0 0\n";
		else if ( s == 0 )
			cout << "-1 -1 \n";
		else
		{
			forn(i,m)
				cout << s2[i];
			cout << " ";
			forn(i,m)
				cout << s1[i];
			cout << "\n";
		}
	}
	return 0;
}



