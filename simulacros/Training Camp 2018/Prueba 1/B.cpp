#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

struct Intervalo
{
	tint start,end,indice;
	Intervalo(tint ss, tint ee, tint ii)
	{
		start = ss;
		end = ee;
		indice = ii;
	}
};

bool operator < (Intervalo i1, Intervalo i2)
{
	return make_pair(i1.start,i2.end) < make_pair(i2.start,i1.end);
}

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint n;
	while (cin >> n)
	{
		vector<Intervalo> v;
		forn(i,n)
		{
			tint l,r;
			cin >> l >> r;
			v.emplace_back(l,r,i+1);
		}
		sort(v.begin(),v.end());
		Intervalo candidato = v[0];
		bool cumple = true;
		forn(i,n)
			cumple &= (candidato.start <= v[i].start) && (v[i].end <= candidato.end);
		if (cumple)
			cout << candidato.indice << "\n";
		else
			cout << "-1\n";
		
	}
	return 0;
}



