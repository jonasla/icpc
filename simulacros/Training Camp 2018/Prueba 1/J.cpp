#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

struct Punto
{
	tint x,y;
	Punto (tint xx, tint yy)
	{
		x = xx;
		y = yy;
	}
};

tint operator ^ (Punto p1, Punto p2)
{
	return p1.x*p2.y - p1.y*p2.x;
}


int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint x1,y1,x2,y2,x3,y3;
	while (cin >> x1 >> y1 >> x2 >> y2 >> x3 >> y3)
	{
		Punto v1 = Punto((x2-x1),(y2-y1));
		Punto v2 = Punto((x3-x2),(y3-y2));
		if ( (v1^v2) < 0)
			cout << "RIGHT\n";
		else if ((v1^v2) > 0)
			cout << "LEFT\n";
		else
			cout << "TOWARDS\n";
	}
	return 0;
}



