#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint n,k;
	while (cin >> n >> k)
	{
		set<tint> divisores;
		tint z = 1;
		while (z*z <= n)
		{
			if (n % z == 0)
			{
				divisores.insert(z);
				divisores.insert(n/z);
			}
			z++;
		}
		tint maxDivisor = -1;
		for (auto d : divisores)
		{
			if (k*(k+1)/2 <= n/d)
				maxDivisor = d;
		}
		if (maxDivisor == -1 or k >= 1000000)
			cout << "-1\n";
		else
		{
			vector<tint> a (k);
			forn(i,k)
				a[i] = i+1;
			a[k-1] = n/maxDivisor - k*(k-1)/2;
			forn(i,k)
			{
				if (i)
					cout << " ";
				cout << maxDivisor*a[i]; 
			}
			cout << "\n";
			
		}
	}
	return 0;
}



