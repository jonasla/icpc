#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

const tint INFINITO = 1000000;

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint n;
	while (cin >> n)
	{
		vector<tint> v (n);
		forn(i,n)
			cin >> v[i];
		reverse(v.begin(),v.end());
		
		tint ans = 0;
		vector<tint> pila = {INFINITO};
		forn(i,n)
		{
			tint r = 0;
			while(pila.back() < v[i])
			{
				pila.pop_back();
				r++;
			}
			ans = max(ans,r);
			pila.push_back(v[i]);
			
		}
		cout << ans << "\n";
			
	}
	return 0;
}



