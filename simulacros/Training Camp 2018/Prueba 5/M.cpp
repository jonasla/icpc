#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

struct Punto
{
	tint x,y,z;
	Punto (tint xx, tint yy, tint zz)
	{
		x = xx;
		y = yy;
		z = zz;
	}
};

Punto operator ^ (Punto p1, Punto p2)
{
	return Punto(p1.y*p2.z-p1.z*p2.y,p1.z*p2.x-p1.x*p2.z,p1.x*p2.y-p2.x*p1.y);
}

tint operator * (Punto p1, Punto p2)
{
	return (p1.x*p2.x + p1.y*p2.y + p1.z*p2.z);  
}

Punto operator - (Punto p1, Punto p2)
{
	return Punto(p1.x-p2.x,p1.y-p2.y,p1.z-p2.z);
}

bool operator == (Punto p1, Punto p2)
{
	return (p1.x == p2.x) && (p1.y == p2.y) && (p1.z == p2.z);
}

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint n;
	while (cin >> n)
	{
		vector<Punto> p (n,Punto(0,0,0));
		forn(i,n)
		{
			tint xx,yy,zz;
			cin >> xx >> yy >> zz;
			p[i] = Punto(xx,yy,zz);
		}
		forsn(i,1,n)
			p[i] = p[i] - p[0];
		p[0] = Punto(0,0,0);
		bool sePuede = true;
		if (n >= 3)
		{
			tint k = 0;
			
			while (k < n && ( ((p[1]-p[0])^(p[k]-p[0])) == Punto(0,0,0)) )
				k++;
			if (k < n)
			{
				//~ debug(k);
				Punto v = ((p[1]-p[0])^(p[k]-p[0]));
				forn(i,n)
				{
					//~ debug(i);
					//~ debug(sePuede);
					sePuede &= ((p[i]*v) == 0);
					
				}
			}
		}
		if (sePuede)
			cout << "TAK\n";
		else
			cout << "NIE\n";
	}
	return 0;
}


