#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

const tint MAXN = 131072;
const tint INFINITO = 1000000000000000;
bitset<MAXN> visitado;
tint c[MAXN];

void dfs (tint actual, vector<vector<tint> > &ladj, tint &mini)
{
	visitado[actual] = 1;
	mini = min(mini,c[actual]);
	for (auto &vecino : ladj[actual])
		if (!visitado[vecino])
			dfs(vecino,ladj,mini);
}

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint n,m;
	while (cin >> n >> m)
	{
		vector<vector<tint> > ladj (n);
		forn(i,n)
			cin >> c[i];
		forn(i,m)
		{
			tint a,b;
			cin >> a >> b;
			ladj[a-1].push_back(b-1);
			ladj[b-1].push_back(a-1);
		}
		visitado.reset();
		tint ans = 0;
		forn(i,n)
			if (!visitado[i])
			{
				tint mini = INFINITO;
				dfs(i,ladj,mini);
				ans += mini;
			}
		cout << ans << "\n";
	}
	return 0;
}



