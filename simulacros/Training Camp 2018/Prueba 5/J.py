import sys

n = int(sys.stdin.readline())
s = sys.stdin.readline()[:-1]

num1,num2,p1,p2 = 1,1,int(2**n),1
for k in range(1,n):
	p1 /= 2
	p2 *= 2
	if s[k-1] == 'H':
		num1 = p1*(num1 + p2) 
		num2 = p1*num2 
		#~ p1 = (p1 + 2**k) 
		#~ p2 = (p2) 
	else:
		num1 = p1*num1  
		num2 = p1*(num2 + p2)
		#~ p1 = (p1) 
		#~ p2 = (p2  + int(2**k))
	#~ print p1,p2
if num1 > num2:
	print 'H'
elif num2 > num1:
	print 'M'
else:
	print 'HM'
	 
		
