#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint n;
	while (cin >> n)
	{
		string s;
		cin >> s;
		vector<tint> r (n);
		forn(i,n)
			if (s[i] == 'H')
				r[i] = 1;
			else
				r[i] = -1;
		vector<tint> exponente(n+100);
		forn(i,n)
		{
			exponente[1] += r[i];
			exponente[n-i] -= r[i];
		}
		forsn(i,1,n+1)
			exponente[i] += exponente[i-1];
		imprimirVector(exponente);
		
	}
	return 0;
}



