#include <bits/stdc++.h>

typedef long long tint;
typedef long double ldouble;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

const tint MAXN = 256;
tint longest[MAXN];
tint entran[MAXN];
tint salen[MAXN];

struct Diamante
{
	ldouble w,c;
	tint ind;
	Diamante (ldouble ww, ldouble cc, tint ii)
	{
		w = ww;
		c = cc;
		ind = ii;
	}
};

bool hay_arista (Diamante &d1, Diamante &d2)
{
	return (d1.w < d2.w) && (d1.c > d2.c) && (d1.ind < d2.ind);
}

tint f (tint i, vector<vector<tint> > &ladj)
{
	if (longest[i] == -MAXN)
	{
		for (auto vecino : ladj[i])
			longest[i] = max(longest[i],f(vecino,ladj)+1);
	}
	return longest[i];
}


int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada2.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint t;
	while (cin >> t)
	{
		forn(caso,t)
		{
			tint n;
			cin >> n;
			vector<Diamante> d (n, Diamante(0,0,0));
			forn(i,n)
			{
				ldouble ww,cc;
				cin >> ww >> cc;
				d[i] = Diamante(ww,cc,i);
			}
			forn(i,n)
			{
				longest[i] = -MAXN;
				entran[i] = 0;
				salen[i] = 0;
			}
			vector<vector<tint> > ladj (n);
			forn(i,n)
			forn(j,n)
			{
				if (hay_arista(d[i],d[j]))
				{
					
					salen[i]++;
					entran[j]++;
					ladj[i].push_back(j);
				}
			} 
			forn(i,n)
				if (salen[i] == 0)
					longest[i] = 1;
			tint ans = 1;
			forn(i,n)
				if (entran[i] == 0)
				{
					ans = max(ans,f(i,ladj));
				}
			
			cout << ans << "\n";
			
		}
	}
	
	return 0;
}



