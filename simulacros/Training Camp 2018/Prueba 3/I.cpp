#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

tint mod (tint n, tint d)
{
	n %= d;
	if (n<0)
		n+=d;
	return n;
}

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada2.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint t;
	while (cin >> t)
	{
		forn(caso,t)
		{
			tint d,n;
			cin >> d >> n;
			vector<vector<tint> > r(d, vector<tint> (d,0));
			forn(i,n)
			{
				tint x,y;
				cin >> x >> y;
				r[mod(x,d)][mod(y,d)]++;
			}
			vector<tint> fila(d);
			vector<tint> columna(d);
			forn(i,d)
			forn(j,d)
			{
				fila[i] += r[i][j];
				columna[j] += r[i][j];
			}
			tint ans = n+1;
			forn(i,d)
			forn(j,d)
				ans = min(ans,fila[i]+columna[j]-r[i][j]);
			cout << ans << "\n";
		}
	}
	return 0;
}



