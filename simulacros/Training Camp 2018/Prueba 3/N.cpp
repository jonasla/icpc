#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint t;
	while (cin >> t)
	{
		forn(caso,t)
		{
			tint m,n;
			cin >> m >> n;
			vector<string> aux (m);
			vector<vector<tint> > v (m, vector<tint> (n));
			forn(i,m)
			{
				cin >> aux[i];
				forn(j,n)
					v[i][j] = (aux[i][j] == '1');
			}
			vector<tint> fila (m,0);
			vector<tint> columna (n,0);
			tint ans = 0;
			forn(i,m)
			dforn(j,n)
			{
				if ( (v[i][j] + columna[j] + fila[i]) % 2 == 0)
				{
					ans++;
					columna[j]++;
					fila[i]++;
				} 	
			}
			cout << ans << "\n";
		}
	}
	return 0;
}



