import sys

alfabeto = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
inv_alfabeto = {alfabeto[i]:i for i in range(len(alfabeto))}
pos = 4
c = int(sys.stdin.readline())
for caso in range(c):
	hist = {x:0 for x in alfabeto}
	l = sys.stdin.readline().split()
	for palabra in l:
		for x in palabra:
			hist[x] += 1
	maxi = max([hist[x] for x in alfabeto])
	candidatos = [x for x in alfabeto if hist[x] == maxi]
	#~ print l
	d_candidatos = []
	for x in candidatos:
		c = inv_alfabeto[x]
		if c >= pos:
			d_candidatos.append((c-pos,x) )
		else:
			d_candidatos.append((len(alfabeto)+c-pos,x) )
		#~ d_candidatos.append((d1-d2,x) )
	#~ print d_candidatos
	ans = []
	if len(d_candidatos) <= 1: 
		d = min(d_candidatos)[0]
		for palabra in l:
			letras = list(palabra)
			for i in range(len(letras)):
				letras[i] = alfabeto[(inv_alfabeto[letras[i]]-d+len(alfabeto))%len(alfabeto)]
			ans.append("".join(letras))
		print d," ".join(ans)
	else:
		print "NOT POSSIBLE"
	
			
		 
