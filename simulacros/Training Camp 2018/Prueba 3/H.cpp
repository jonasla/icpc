#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

struct Auto
{
	tint precio,capacidad;
	Auto (tint pp, tint cc)
	{
		precio = pp;
		capacidad = cc;
	}
};

bool operator < (Auto a1, Auto a2)
{
	return make_pair(a1.precio,a2.capacidad) < make_pair(a2.precio,a1.capacidad);
}

bool llega (Auto car, deque<tint> &estaciones, tint t)
{
	tint r = tint(estaciones.size());
	bool llego = true;
	tint suma = 0;
	forn(i,r-1)
	{
		
		tint d = estaciones[i+1] - estaciones[i];
		if (car.capacidad >= d)
		{
			if (car.capacidad >= 2*d)
				suma += d;
			else
				suma += 3*d - car.capacidad;
		}
		else
		{
			llego = false;
			break;
		}
	}
	return (llego && (suma <= t));
}

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada2.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint n,k,s,t;
	while (cin >> n >> k >> s >> t)
	{
		vector<Auto> v (n,Auto(0,0));
		forn(i,n)
		{
			tint pp,cc;
			cin >> pp >> cc;
			v[i] = Auto(pp,cc);
		}
		
		deque<tint> estaciones (k);
		forn(i,k)
			cin >> estaciones[i];
		estaciones.push_front(0);
		estaciones.push_back(s);
		sort(estaciones.begin(),estaciones.end());
			
		sort(v.begin(),v.end());
		vector<Auto> candidatos;
		tint maxCapacidad = -1;
		forn(i,n)
		{
			if (v[i].capacidad > maxCapacidad)
			{
				candidatos.push_back(v[i]);
				maxCapacidad = v[i].capacidad;
			}
		}
		
		tint r = tint(candidatos.size());
		tint a = -1, b = r-1; // con b llegan
		//~ cout << candidatos[b].precio << " " << candidatos[b].capacidad << endl;
		if (!llega(candidatos[b],estaciones,t))
			cout << "-1\n";
		else
		{
			while (b-a>1)
			{
				tint c = (a+b)/2;
				if (llega(candidatos[c],estaciones,t))
					b = c;
				else
					a = c;
			}
			cout << candidatos[b].precio << "\n";
		}
		
			
	}
	return 0;
}



