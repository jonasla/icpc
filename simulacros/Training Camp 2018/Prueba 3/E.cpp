#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

const tint MAXN = 1024;
tint h[MAXN];

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint t;
	while (cin >> t)
	{
		forn(caso,t)
		{
			forn(i,MAXN)
				h[i] = 0;
			tint n;
			cin >> n;
			forn(i,n)
			{
				tint x;
				cin >> x;
				h[x]++;
			}
			tint maxi = *max_element(h,h+MAXN);
			tint ans = -1;
			forn(x,MAXN)
			{
				if (h[x] == maxi)
				{
					ans = x;
					break;
				}
			}
			cout << ans << "\n";
		}
	}
	return 0;
}



