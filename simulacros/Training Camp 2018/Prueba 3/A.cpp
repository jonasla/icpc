#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

void imprimirVector (vector<int> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

pair<vector<int>,string> enRomano(int n)
{
	vector<int> numero;
	string romano;
	numero.push_back(n);
	
	int caracteres = 0;
	int M = 0;
	int m = 0;
	int CM = 0;
	int D = 0;
	int d = 0;
	int CD = 0;
	int C = 0;
	int c = 0;
	int XC = 0;
	int L = 0;
	int l = 0;
	int XL = 0;
	int X = 0;
	int x = 0;
	//~ int IX = 0;
	int V = 0;
	int v = 0;
	//~ int IV = 0;
	int I = 0;
	int i = 0;
	
	while (n>0)
	{
		if (n>=1000 && M<3)
		{
			n -= 1000;
			M++;
			caracteres++;
			romano.push_back('m');
		} 
		else if (n>=900 && CM==0)
		{
			n -= 900;
			CM++;
			m++;
			c++;
			caracteres = caracteres + 2;
			romano.push_back('c');
			romano.push_back('m');
		} 
		else if (n>=500 && CM==0 && D==0)
		{
			n -= 500;
			D++;
			caracteres++;
			romano.push_back('d');
		} 
		else if (n>=400 && CM==0 && CD == 0)
		{
			n -= 400;
			CD++;
			c++;
			d++;
			caracteres = caracteres + 2;
			romano.push_back('c');
			romano.push_back('d');
		} 
		else if (n>=100 && CM==0 && CD == 0 && C<3)
		{
			n -= 100;
			C++;
			caracteres++;
			romano.push_back('c');
		}
		else if (n>=90 && XC==0)
		{
			n -= 90;
			XC++;
			x++;
			c++;
			caracteres = caracteres +2;
			romano.push_back('x');
			romano.push_back('c');
		}
		else if (n>=50 && L==0 && XC == 0)
		{
			n -= 50;
			L++;
			caracteres++;
			romano.push_back('l');
		}
		else if (n>=40 && XL==0 && L == 0 && XC==0)
		{
			n -= 40;
			XL++;
			x++;
			l++;
			caracteres = caracteres + 2;
			romano.push_back('x');
			romano.push_back('l');
		}
		else if (n>=10 && X<3 && XL == 0 && XC == 0)
		{
			n -= 10;
			X++;
			caracteres++;
			romano.push_back('x');
		}
		else if (n == 9)
		{
			n=0;
			I++;
			X++;
			caracteres = caracteres + 2;
			romano.push_back('i');
			romano.push_back('x');
		}
		else if (n == 8)
		{
			n=0;
			I = I+3;
			V++;
			caracteres = caracteres + 4;
			romano.push_back('v');
			romano.push_back('i');
			romano.push_back('i');
			romano.push_back('i');
		}
		else if (n == 7)
		{
			n=0;
			I = I+2;
			V++;
			caracteres = caracteres + 3;
			romano.push_back('v');
			romano.push_back('i');
			romano.push_back('i');
		}
		else if (n == 6 || n ==4)
		{
			n=0;
			I += 1;
			V++;
			caracteres = caracteres + 2;
			romano.push_back('v');
			romano.push_back('i');
		}
		else if (n == 5)
		{
			n=0;
			V++;
			caracteres++;
			romano.push_back('v');
		}
		else if (n < 4)
		{
			n--;
			I++;
			caracteres++;
			romano.push_back('i');
		}
	}
	
	numero.push_back(caracteres);
	numero.push_back(M+m);
	numero.push_back(D+d);
	numero.push_back(C+c);
	numero.push_back(L+l);
	numero.push_back(X+x);
	numero.push_back(V+v);
	numero.push_back(I+i);
	
	return {numero,romano};
}

const tint cota = 4000;

int main()
{
	
	#ifdef ACMTUYO
		assert(freopen("entrada2.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	string s;
	while (cin >> s)
	{
		map<char,tint> conv = {{'m',0},{'d',1},{'c',2},{'l',3},{'x',4},{'v',5},{'i',6}};
		vector<int> hist(7);
		for (auto x : s)
		{
			//~ debug(x);
			//~ debug(char(tolower(x)));
			//~ debug(conv[char(tolower(x))]);
			
			if (conv.find(tolower(x)) != conv.end())
				hist[conv[char(tolower(x))]]++;
		}
		tint maxLargo = 0;
		string ans = "";
		forsn(i,1,cota)
		{
			//~ debug(i);
			pair<vector<int>,string> numero_romano = enRomano(int(i));
			if (numero_romano.first[1] >= maxLargo)
			{
				bool todo_bien = true;

				forsn(k,2,9)
					todo_bien &= (hist[k-2] >= numero_romano.first[k]);
				if (todo_bien)
				{
					//~ imprimirVector(hist);
					//~ imprimirVector(numero_romano.first);
					maxLargo = numero_romano.first[1];
					ans = numero_romano.second;
				}
			}
		}
		
		for (auto &x : ans)
			x = toupper(x);
		cout << ans << "\n";
		
	}
	
	
	
	//~ imprimirVector(enRomano(16).first);
	//~ cout << enRomano(16).second << endl;
	return 0;
}



