#include <bits/stdc++.h>

typedef long long tint;


#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

tint LETRAS_ALFABETO = 26; // alfabeto ingles de 'a' - 'z'

tint MAX_LETRAS = 52; // Notar que el enunciado dice que pueden 
					  // ser mayusculas y minusculas.
					  // Aunque en los ejemplos son todas minusculas.
/****************
* Nodo del Trie *
****************/

struct Trie 
{
	tint cantidadPrefijos; 	// Indica la cantidad de prefijos que  
							// utilizan al nodo
	map<char,Trie> hijos;	// map con los hijos del nodo.
	Trie() 		// Inicializacion de un nodo
	{
		cantidadPrefijos = 0;
		hijos = {};	// Un nodo que no fue creado, no tiene hijos
	}
};


/*******************
* Funcion Auxiliar *
*******************/

// 'a' - 'z' -> 0-25
// 'A' - 'Z' -> 26-51

tint char2Number (char x)
{
	if (x < 'a') 	// es mayuscula
		return LETRAS_ALFABETO + x - 'A';
	else 			// es minuscula
		return x - 'a';
}

/*********************
* Funciones del Trie *
*********************/

/* Como precondicion de "agregarPalabra" se tiene que:
  
 1) Para agregar una palabra entera, se debe llamar con "trie" como el 
 * nodo raiz del trie, e "indice" igual a cero.
 2) 0 <= indice <= palabra.size()

*/ 

void agregarPalabra (Trie &trie, string &palabra, tint indice)
{
	tint n = palabra.size();
	
	if (indice == n)
		trie.cantidadPrefijos++;
	if (indice < n)
		agregarPalabra(trie.hijos[palabra[indice]],palabra,indice+1);
}

/* Como precondicion de "prefijosPalabra" se tiene que:
  
 * 1) La palabra tiene que haber sido agregada antes al Trie
 * 2) 0 <= indice <= palabra.size()

*/ 

tint buscarPalabra (Trie &trie, string &palabra, tint indice)
{
	tint n = palabra.size();
	if (indice < n)
		return buscarPalabra(trie.hijos[palabra[indice]],palabra,indice+1);
	else
		return trie.cantidadPrefijos;
	
}


int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada2.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	tint t;
	while (cin >> t)
	{
		forn(caso,t)
		{
			tint n;
			cin >> n;
			vector<vector<string> > c (2,vector<string> (n));
			forn(k,2)
			forn(i,n)
				cin >> c[k][i];
			Trie t1 = Trie();
			Trie t2 = Trie();
			vector<tint> ans;
			tint suma = 0, last = 0;
			forn(i,n)
			{
				agregarPalabra(t1,c[0][i],0);
				agregarPalabra(t2,c[1][i],0);
				suma += buscarPalabra(t1,c[1][i],0);
				suma += buscarPalabra(t2,c[0][i],0);
				suma -= (c[1][i] == c[0][i]);
				//~ debug(suma);
				//~ debug(last);
				//~ debug(i);
				
				if (suma == i - last + 1 )
				{
					t1 = Trie();
					t2 = Trie();
					ans.push_back(i-last+1);
					suma = 0;
					last = i+1;
					
				}
			}
			tint r = ans.size();
			forn(i,r)
			{
				if (i)
					cout << " ";
				cout << ans[i];
			}
			cout << "\n";
			//~ cout << "------\n";
		}
		
	}
	return 0;
}



