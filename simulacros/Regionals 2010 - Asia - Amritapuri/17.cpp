#include <iostream>
#include <vector>
#include <algorithm>
#include <bitset>

using namespace std;

typedef long long tint;

#define forsn(i,s,n) for(tint i = (tint)(s) ;  i < (tint)(n); i++)
#define forn(i,n) for(tint i = (tint)(0) ;  i < (tint)(n); i++)

const tint maxN = 1024;
const tint nmod = 1000000007;

tint binom[maxN][maxN];

tint comb(tint n, tint m)
{
	if (m < 0 || m > n)
		return 0;
	if (m == 0 || m == n)
		return 1;
	if (binom[n][m] == -1)
		binom[n][m] = (comb(n-1,m-1) + comb(n-1,m)) % nmod;
	return binom[n][m];
}

vector<tint> caminito;
tint representante[maxN];
tint tamanho[maxN];

void inicializar (tint n) {
	forn(i,n) {
		representante[i] = i;
		tamanho[i] = 1;
	}
}

tint find (tint x) {
	caminito = {};
	while (x != representante[x]) {
		caminito.push_back(x);
		x = representante[x];	
	}
	for (auto z : caminito)
		representante[z] = x;
	return x;
}

bool same (tint a, tint b) { return (find(a) == find(b)); }

void unite (tint a, tint b) {
	a = find(a);
	b = find(b);
	if (tamanho[a] < tamanho[b])
		swap(a,b);
	tamanho[a] += tamanho[b];
	representante[b] = a;	
}

int main()
{
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	forn(i,maxN)
	forn(j,maxN)
		binom[i][j] = -1;
	tint t;
	cin >> t;
	forn(caso,t)
	{
		tint n,k;
		cin >> n >> k;
		inicializar(n);
		forn(i,n)
		{
			tint c;
			cin >> c;
			unite(i,c);
		}
		bitset<maxN> usado;
		vector<tint> p;
		tint ans = 1;
		forn(i,n)
			if (!usado[find(i)])
			{
				usado[find(i)] = 1;
				cout << tamanho[find(i)] << endl;
				ans = ( ans * comb(k,tamanho[find(i)]) ) % nmod;
			}
		
		cout << ans << "\n";
		cout << "-------------\n";
	}
	return 0;
}
