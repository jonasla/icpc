#include <iostream>
#include <cmath>
#include <algorithm>
#include <vector>
#include <map>
#include <iomanip>
#include <utility>
#include <tuple>


using namespace std;

typedef long long tint;
typedef long double ldouble;

#define forn(i,n) for (tint i = 0; i < (tint)(n); i++)

const ldouble pi = acos(-1);
const ldouble INFINITO = 1e18;
const tint maxN = 5050;

struct Punto
{
	ldouble px,py,x,y;
	tint ipx,ipy,ix,iy,id;
	Punto (tint idd, ldouble xx, ldouble yy)
	{
		id = idd;
		x = xx;
		y = yy;
		py = sqrt(xx*xx+yy*yy);
		px = atan2(y,x) + pi;	// 0 ... 2pi
	}
	Punto()
	{
	}
};

Punto p[maxN];
tint tablita[maxN][maxN];


bool porPX (Punto p1, Punto p2)
{
	return make_tuple(p1.px, p2.py) < make_tuple(p2.px, p1.py);
}

bool porPY (Punto p1, Punto p2)
{
	return make_tuple(p1.py, p2.px) < make_tuple(p2.py, p1.px);
}

ldouble f (ldouble d, ldouble r)
{
	return r*r*d/2.0;
}

tint puntosEnRectangulo(tint  minIndicePX, tint maxIndicePX, tint maxIndicePY)
{
	//~ cout << "HOLA2" << endl;
	//~ cout << minIndicePX << " " << maxIndicePX << " " << maxIndicePY << endl;
	tint r = tablita[maxIndicePX][maxIndicePY] - tablita[minIndicePX-1][maxIndicePY];
	//~ cout << r << endl;
	return r;
} 

int main()
{
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	tint n,k;
	tint caso = 1;
	while (cin >> n >> k && n != 0)
	{
		
		forn(i,n)
		{
			tint xx,yy;
			cin >> xx >> yy;
			if (xx == 0 && yy == 0) {
				k--;
				n--;
				i--;
				continue;
			}
			p[i] = Punto(i,xx,yy);
		}

		
		sort(p,p+n,porPY);
		forn(i,n)
		{
			p[i].ipy = i+1;
			//~ cout << p[i].id << " ";
		}
		//~ cout << endl;
		sort(p,p+n,porPX);
		forn(i,n)
		{
			p[i].ipx = i+1;
			//~ cout << p[i].id << " ";
		}
		//~ cout << endl;
		
		forn(i,n+1)
		forn(j,n+1)
			tablita[i][j] = 0;
		forn(i,n)
			tablita[p[i].ipx][p[i].ipy] = 1;
		

		forn(i,n)
		forn(j,n)
			tablita[i+1][j+1] += tablita[i][j+1] + tablita[i+1][j] - tablita[i][j];

		//~ forn(i,n+1)
		//~ {
			//~ forn(j,n+1)
				//~ cout << tablita[i][j] << " ";
			//~ cout << endl;
		//~ }
		//~ cout << endl;
		
		
		ldouble minArea = INFINITO;
		forn(i,n)
		forn(j,i+1)
		{
			tint maxIndicePY = max(p[i].ipy,p[j].ipy);
			ldouble maxR = max(p[i].py,p[j].py);
			tint minIndicePX = min(p[i].ipx,p[j].ipx);
			tint maxIndicePX = max(p[i].ipx,p[j].ipx);

			ldouble d = abs(p[i].px-p[j].px);

			//~ if (p[j].id == 0 && p[i].id == 3) {
				//~ cout << minArea << endl;
				//~ cout << minIndicePX << endl;
				//~ cout << maxIndicePX << endl;
				//~ cout << maxIndicePY << endl;				
				//~ cout << d << endl;
			//~ }
			//~ cout << "puntos " << p[j].id << " " << p[i].id << endl;
			if (puntosEnRectangulo(minIndicePX,maxIndicePX,maxIndicePY) >= k)
			{
				
				//~ cout << "HOLA" << endl; 
				minArea = min(minArea,f(d,maxR));
				//~ cout << "tiene interno\n";
				//~ cout << minArea << endl;
			}
			else if (puntosEnRectangulo(1,minIndicePX,maxIndicePY) + puntosEnRectangulo(maxIndicePX,n,maxIndicePY) >= k)
			{
				//~ cout << "HOLA" << endl;
				
				minArea = min(minArea,f(2*pi-d,maxR));
				//~ cout << "tiene externo\n";
				//~ cout << minArea << endl;
			}
			//~ if (p[j].id == 0 && p[i].id == 3) {
				//~ cout << minArea << endl;
			//~ }			
		}
		cout << "Case #" << caso++ << ": " << fixed << showpoint << setprecision(2) << minArea << "\n";
		//~ caso++;
		
	}
	return 0;
}
