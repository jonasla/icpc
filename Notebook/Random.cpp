#include <iostream>
#include <random>
#include <iomanip>

using namespace std;

random_device rd;
mt19937 gen(rd());

uniform_int_distribution<int> dis1(1, 10000);
uniform_real_distribution<long double> dis2(1, 10000);

int main()
{
	
	cout <<  dis1(gen) << "\n";
	cout <<  fixed << showpoint << setprecision(5) << dis2(gen) << "\n";
	cout << setfill('0') << setw(10) << 12345 << "\n"; // 0000012345
	return 0;
}

