#include <iostream>
#include <vector>
#include <algorithm>


#define forn(i,n) for(tint i=0;i<(tint)(n);i++)

typedef long long tint;
tint nmod = 1000000007; // o el primo que deseamos

using namespace std;

vector<tint> desarrollo (tint n) // En des queda guardado el desarrollo dado vuelta, ya que necesitamos iterar desde las unidades en invMod
{
	vector<tint> des;
	while (n > 0)
	{
		des.push_back(n%2);
		n /= 2;
	}
	return des;
}

tint potLogMod (tint x, tint y) // Calcula: (x^y) mod nmod
{
	tint ans = 1;
	while (y > 0)
	{
		if (y % 2)
			ans = (x * ans) % nmod;
		x = (x * x) % nmod;
		y /= 2;
	}
	return ans;
}

tint invLog(tint a) // Solo funciona si nmod es primo y devuelve un numero b tal que: (a*b) = 1 mod nmod 
{
	return potLogMod(a,nmod-2);
}

int main()
{
	return 0;
}
