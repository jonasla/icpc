#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>

using namespace std;

typedef int tint;

#define forn(i,n) for (tint  i = 0; i < (tint) (n) ; i++)

const tint maxN = 131072;

vector<tint> caminito;

tint representante[maxN];
tint tamanho[maxN];

void inicializar (tint n)
{
	forn(i,n)
	{
		representante[i] = i;
		tamanho[i] = 1;
	}
}

tint find (tint x)
{
	caminito = {};
	while (x != representante[x])
	{
		caminito.push_back(x);
		x = representante[x];	
	}
	for (auto z : caminito)
		representante[z] = x;
	return x;
}

bool same (tint a, tint b)
{
	return (find(a) == find(b));
}

void unite (tint a, tint b)
{
	a = find(a);
	b = find(b);
	if (tamanho[a] < tamanho[b])
		swap(a,b);
	tamanho[a] += tamanho[b];
	representante[b] = a;	
}


int main()
{
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	inicializar(10);
	
	return 0;
}
