#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <cmath>
#include <algorithm>
#include <cassert>
#include <tuple>
#include <functional>
#include <unordered_set>
#include <unordered_map>
#include <sstream>
#include <stdio.h>
#include <valarray>
#include <random>
#include <iomanip>


typedef long long tint;
typedef unsigned long long utint;
typedef long double ldouble; 


#define forn(i,n) for(tint i=0;i<(tint)(n); i++)
#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define esta(x,v) (find((v).begin(),(v).end(),(x)) !=  (v).end())
#define index(x,v) (find((v).begin(),(v).end(),(x)) - (v).begin())
#define debug(x) cout << #x << " = "  << x << endl



using namespace std;



void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}

tint toNumber (string s)
{
	tint Number;
	if ( ! (istringstream(s) >> Number) )
		Number = 0; // el string vacio lo manda al cero
	return Number;
}

string toString (tint number)
{    
    ostringstream ostr;
    ostr << number;
    return  ostr.str();
}

bool esPalindromo (tint i, tint j, vector<vector<tint> > &r, tint n) // asumo i < j
{
	if (i+j >= n)
		return (r[i+j][n-i] - r[i+j][n-j-1]) == j-i+1;
	else
		return (r[i+j][j+1] - r[i+j][i]) == j-i+1;
}

int main()
{
	#ifdef ACMTUYO
		assert(freopen("entrada.in", "r", stdin));
	#endif
	ios_base::sync_with_stdio(0);
	cin.tie(NULL);
	string s;
	while (cin >> s)
	{
		tint n = s.size();
		vector<vector<tint> > v (n, vector<tint> (n,0));
		forn(i,n)
		forn(j,n)
			v[i][j] = (s[i] == s[j]);
		vector<vector<tint> > r (2*n-1,vector<tint> (n+1,0));
		
		forn(i,2*n-1)
		{
			tint sum = 0, x = min(i,n-1), y = 0;
			if (i >= n)
				y = i-n+1;
			forn(j,n)
			{
				if (x >=0 && y < n)
					sum += v[x--][y++];
				r[i][j+1] = sum;
			}
		}
		
			forn(j,n)
		{
			forn(i,j+1)
				cout << esPalindromo(i,j,r,n) << " ";
			cout << endl;
		}
		cout << endl;
		forn(i,2*n-1)
		{
			forn(j,n+1)
				cout << r[i][j] << " ";
			cout << endl;
		}	
		
		cout << endl;
		
		
		
	}
	return 0;
}




