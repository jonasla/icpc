#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
#include <tuple>
#include <bitset>
#include <cassert>

using namespace std;

typedef int tint;

#define forn(i,n) for (tint  i = 0; i < (tint) (n) ; i++)
#define forsn(i,s,n) for (tint  i = (tint)(s); i < (tint) (n) ; i++)

const tint maxN = 32768; // cantidad de nodos
const tint maxK = 16;	 // lg(cantidadDeNodos)
const tint NEUTRO = 1e8; // neutro de la operacion que queremos hacer (ejemplo: minimo)

tint d[maxN]; // profundidad
pair<tint,tint> p[maxN][maxK]; // ancestro a distancia 2**k, Lo que queremos entre los 2**k ancestros

void dfs(tint actual, vector<vector<pair<tint,tint> > > &ladj, tint padre)
{
	d[actual] = d[padre]+1;
	for (auto x : ladj[actual])
		if (x.first != padre)
		{
			p[x.first][0] = {actual,x.second};
			dfs(x.first,ladj,actual);
		}
	
}

tint subir(tint a, tint c, tint &ans, bool tomaMinimo)
{
	tint k = 0;
	while (c > 0)
	{
		if (c % 2)
		{
			if (tomaMinimo)
				ans = min(ans,p[a][k].second);
			a = p[a][k].first;
		}
		k++;
		c /= 2;
	}
	return a;
}

tint answer (tint a, tint b)
{
	// IGUALAMOS LAS ALTURAS
	if (d[a] < d[b])
		swap(a,b);
	tint w = d[a] - d[b], ans = NEUTRO;
	a = subir(a,w,ans,true);
	
	// HACEMOS LA BINARY PARA BUSCAR EL LCA
	tint cInf = 0, cSup = maxN; 
	while (cSup - cInf > 1)
	{
		tint ra = a, rb = b;
		tint c = (cSup+cInf)/2;
		ra = subir(ra,c,ans,false);
		rb = subir(rb,c,ans,false);
		if (ra == rb)
			cSup = c;
		else
			cInf = c;
	}
	// SUBIMOS LO QUE HAGA FALTA PARA LLEGAR AL LCA
	cSup *= (a != b);
	a = subir(a,cSup,ans,true);
	b = subir(b,cSup,ans,true);
	return ans;
}

int main()
{
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	forn(i,maxN)
	forn(k,maxK)
		p[i][k] = {-1,NEUTRO};

	vector<vector<pair<tint,tint> > > ladj (maxN); // listaDeAdyacencia del arbol
	d[0] = -1;
	dfs(0,ladj,0);
	// LLENADO DE LA TABLA
	forsn(k,1,maxK)	
	forn(i,maxN)
	{
		tint ancestro = p[i][k-1].first;
		if (ancestro >= 0)
			p[i][k] = {p[ancestro][k-1].first, min(p[i][k-1].second,p[ancestro][k-1].second) };
	}
}
