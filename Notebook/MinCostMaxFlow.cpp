// C++11
#include <bits/stdc++.h>

#define forsn(i,s,n) for(tint i=(s);i<(tint)(n); i++)
#define forn(i,n) forsn(i,0,n)
#define dforn(i,n) for(tint i = tint(n)-1; i >= 0; i--)
#define debug(x) cout << #x << " = "  << x << endl

using namespace std;

typedef long long tint;

const tint INF = 1000000000000000000;


void imprimirVector (vector<tint> v)
{
	if (!v.empty())
	{ 
		tint p = tint(v.size());
		cout << "[";
		forn(i,p-1)
			cout << v[i] << ",";
		cout << v[p-1] << "]" << endl;
	}
	else
		cout << "[]" << endl;
}


struct Edge
{
	tint start,end,capacity,flow,cost,index;
	Edge (tint ss, tint ee, tint cc, tint ff, tint cco, tint ii)
	{
		start = ss;
		end = ee;
		capacity = cc;
		flow = ff;
		cost = cco;
		index = ii;
	}
};

const tint MAXN = 512;
vector<tint> adj_list[MAXN];

struct Network
{
	vector<Edge> edge_list;
	set<tint> vertices;
	tint source, sink;
	Network(tint ss, tint tt)
	{
		source = ss;
		sink = tt;
		edge_list = {};
		vertices = {};
	}
	
	void add_vertex(tint x)
	{
		vertices.insert(x);
	}
	
	void add_edge_aux(Edge edge)
	{
		vertices.insert(edge.start);
		vertices.insert(edge.end);
		edge_list.emplace_back(edge);
		adj_list[edge.start].emplace_back(edge.index);
	}
	
	void add_edge(tint ss, tint ee, tint cap, tint co)
	{
		add_edge_aux(Edge(ss,ee,cap,0, co,tint(edge_list.size())));
		add_edge_aux(Edge(ee,ss, 0 ,0,-co,tint(edge_list.size())));
	}
	
	tint min_edge_capacity(vector<tint> &parent)
	{
		tint min_cap = INF;
		tint actual = sink;
		while (actual != source)
		{
			Edge edge = edge_list[parent[actual]];
			min_cap = min(min_cap,edge.capacity - edge.flow);
			actual = edge.start;
		}
		return min_cap;
	}
	
	tint update_path(vector<tint> &parent, tint min_cap)
	{
		tint actual = sink;
		tint total_cost = 0;
		while (actual != source)
		{
			edge_list[parent[actual]].flow += min_cap;
			edge_list[parent[actual]^1].flow -= min_cap;
			total_cost += min_cap*edge_list[parent[actual]].cost;
			actual = edge_list[parent[actual]].start;
		}
		return total_cost;
	}
	
	pair<tint,tint> shortest_path_spfa(vector<tint> &distance)
	{
		vector<tint> parent;
		for (const auto &x : vertices)
		{
			distance[x] = INF;
			parent.push_back(-1);
		}
		
		distance[source] = 0;
		vector<tint> layer = {source}, new_layer = {};
		while (!layer.empty())
		{
			for (const auto &v : layer)
			{
				for (const auto &edge_index : adj_list[v])
				{
					Edge edge = edge_list[edge_index];
					if (edge.flow < edge.capacity && distance[edge.start] + edge.cost < distance[edge.end])
					{
						distance[edge.end] = distance[edge.start] + edge.cost;
						parent[edge.end] = edge_index;
						new_layer.push_back(edge.end);
					}
				}
			}
			layer = {};
			layer.swap(new_layer);
		}
		if (distance[sink] < INF)
		{
			tint min_cap = min_edge_capacity(parent);
			tint total_cost = update_path(parent,min_cap);
			return make_pair(min_cap,total_cost);
		}
		else
			return make_pair(-1,-1);
	}
	
	
	pair<tint,tint> max_flow_min_cost ()
	{
		tint total_flow = 0, total_cost = 0;
		vector<tint> distance(tint(vertices.size()),INF);
		bool is_connected = true;
		while (is_connected)
		{
			pair<tint,tint> flow_and_cost = shortest_path_spfa(distance);
			is_connected &= (flow_and_cost.first > 0);
			if (is_connected)
			{
				total_flow += flow_and_cost.first;
				total_cost += flow_and_cost.second;
			}
		}
		return make_pair(total_flow,total_cost);
	}
	
	
};

struct Person
{
	tint height,index,original_index;
	Person(tint hh, tint ii, tint oo)
	{
		height = hh;
		index = ii;
		original_index = oo;
	}
};

tint sqr(tint x)
{
	return x*x;
}

const tint MAXTIME = 920; // MAXTIME*0.001
tint start_clock,end_clock;


bool check_time()
{
	end_clock =  tint(clock());
	return 100*(end_clock-start_clock) <= MAXTIME*CLOCKS_PER_SEC;
}

class LineUp {
public:
    vector<int> getLineup(vector<int> heights, vector<int> arrangements) 
    {
		start_clock = tint(clock());
		tint n = tint(heights.size());
		tint x = tint(arrangements.size())/n;
		vector<Person> performer(n,Person(0,0,0));
		tint sum = 0;
		forn(i,n)
		{
			performer[i] = Person(heights[i],i,i);
			sum += heights[i];
		}
		//~ debug(sum);
		vector<int> ret(n*x);
		forn(show,x)
		{
			
			tint N = 2*n+2;
			forn(i,N)
				adj_list[i] = {};
			Network network = Network(0,2*n+1);
			forn(i,n)
			{
				network.add_edge(0,i+1,1,0);
				forn(j,n)
					network.add_edge(i+1,n+j+1,1,(2*sum*x/n)*abs(performer[i].index-j) + sqr(performer[i].height - tint(arrangements[show*n+j])));
				network.add_edge(n+i+1,2*n+1,1,0);
			}
			
			pair<tint,tint> result = network.max_flow_min_cost();
			for (const auto &edge : network.edge_list)
			{
				
				if (1 <= edge.start && edge.start <= n && edge.flow > 0)
				{
					ret[show*n+(edge.end-n-1)] = int(performer[edge.start-1].original_index);
					performer[edge.start-1].index = edge.end-n-1;
				}
			}
		}
        
        return ret;
    }
};
// -------8<------- end of solution submitted to the website -------8<-------

template<class T> void getVector(vector<T>& v) {
    for (int i = 0; i < v.size(); ++i)
        cin >> v[i];
}

int main() {
    LineUp sol;
    int size;
    cin >> size;
    vector<int> heights(size);
    getVector(heights);
    cin >> size;
    vector<int> arrangements(size);
    getVector(arrangements);
    vector<int> ret = sol.getLineup(heights, arrangements);
    cout << ret.size() << endl;
    for (int i = 0; i < ret.size(); i++)
      cout << ret[i] << endl;
    cout.flush();
}
