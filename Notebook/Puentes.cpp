
const tint INFINITO = 1e8;


struct Arista
{
	tint end,soyPuente,indice,grado1;
	Arista (tint ee, tint ss, tint ii, tint gg)
	{
		end = ee;
		soyPuente = ss;
		indice = ii;
		grado1 = gg;
	}
};

tint timer,n;
const tint maxN = 131072;
bool usado[maxN];
tint highest[maxN], tIn[maxN], tOut[maxN]; 

void bridges (tint actual, tint padre, vector<vector<Arista> > &ladj, vector<tint> &puente)
{
	usado[actual] = true;
	tIn[actual] = timer;
	highest[actual] = timer;
	timer++;
	bool yaIgnore = false;
	for (auto &arista : ladj[actual])
	{
		tint vecino = arista.end;
		if (vecino != padre || yaIgnore)
		{
			if (usado[vecino])
				highest[actual] = min(highest[actual],tIn[vecino]);
			else
			{
				bridges(vecino,actual,ladj,puente);
				highest[actual] = min(highest[actual],highest[vecino]);
				if (highest[vecino] > tIn[actual])
					puente[arista.indice] = 1;
			}
		}
		else
			yaIgnore = true;
	}
	tOut[actual] = timer++;
}

void find_bridges(vector<vector<Arista> > &ladj, vector<tint> &puente)
{
    timer = 0;
    forn(i,n)
        usado[i] = false;
    forn(i,n)
        if (!usado[i])
            bridges(i,-1,ladj,puente);
}
