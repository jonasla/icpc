const tint MAXN = 1LL << 18;

tint sz[MAXN]; // size of subtree including v
vector<tint> g[MAXN]; // adjacency list

// subtree v : [in[v],out[v])
// path v    : [in[next[v]],in[v]]
tint in[MAXN]; 
tint rin[MAXN]; // valor[rin[i]] para generar segtree 
tint out[MAXN]; 
tint nxt[MAXN]; // ancestro en heavy path
tint h[MAXN]; // altura
tint p[MAXN]; // padre
tint t;

void dfs_sz(tint v)
{
    sz[v] = 1;
    for(auto &u: g[v])
    {
		h[u] = h[v] + 1;
		p[u] = v;
        dfs_sz(u);
        sz[v] += sz[u];
        if(sz[u] > sz[g[v][0]])
            swap(u, g[v][0]);
    }
}

void dfs_hld(tint v)
{
    in[v] = t++;
    rin[in[v]] = v;
    for(auto u: g[v])
    {
        nxt[u] = (u == g[v][0] ? nxt[v] : u);
        dfs_hld(u);
    }
    out[v] = t;
}

tint lca(tint u, tint v)
{
	while (nxt[u] != nxt[v])
	{
		if (h[nxt[u]] > h[nxt[v]])
			u = p[nxt[u]];
		else
			v = p[nxt[v]];
	}
	if (h[u] < h[v])
		return u;
	else
		return v;
}






int main()
{
	/// USO
	tint casos;
	cin >> casos;
	forn(caso,casos)
	{
		tint n;
		cin >> n;
		forn(i,n)
		{
			g[i] = {};
			tint m;
			cin >> m;
			forn(arista,m)
			{
				tint vecino;
				cin >> vecino;
				vecino--;
				g[i].push_back(vecino);
			}
		}
		h[0] = 0; // 0 es raiz, tiene altura 0
		p[0] = 0; // 0 es su propio padre
		dfs_sz(0);// 0 es raiz
		dfs_hld(0); // 0 es raiz
		tint q;
		cin >> q;
		cout << "Case " << caso+1 <<":\n";
		forn(query,q)
		{
			tint u,v;
			cin >> u >> v;
			u--;
			v--;
			cout << lca(u,v) + 1 << "\n";
		}
	}
	return 0;
}



